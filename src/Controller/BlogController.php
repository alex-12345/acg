<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    #[Route('/blogs', methods:["GET"])]
    public function index(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request, 12);

        $blogRepository = $doctrine->getRepository(Blog::class);
        $paginator = $blogRepository->getListPaginator($paginateHelper);

        $json_blocks =  $jsonBlockLoader->load($doctrine);

        return $this->render('user/blogs/index.html.twig', [
            'json_blocks' => $json_blocks,
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "blogs", "article"),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3) ,
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'blogs' => $paginator 
        ] + $paginateHelper -> getViewInfo($blogRepository->getNum(), 8) );
    }

    #[Route('/blogs/{id}/{slug}', methods:["GET"], name:'blog_view')]
    public function view(int $id, string $slug, Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $repository = $doctrine->getRepository(Blog::class);
        $blog = $repository->find($id);

        if(!$blog)
        { 
            throw $this->createNotFoundException();
        } else if( $blog->getCanonicalUrl() !== $slug)
        {
            return $this->redirectToRoute('blog_view', ['id'=>$id, 'slug'=> $blog->getCanonicalUrl()], Response::HTTP_MOVED_PERMANENTLY);
        }


        return $this->render('user/blogs/view.html.twig', [
            'seo' => SEOHelper::getSEOFromData($blog->getTitle(), $blog->getDescription(), "blogs/".$id."/".$blog->getCanonicalUrl(), "article"),
            'json_blocks' => $jsonBlockLoader->load($doctrine),
            'blog' => $blog,
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
        ]);
    }



    #[Route('/admin/blogs', name: 'admin_blogs', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $blogRepository = $doctrine->getRepository(Blog::class);
        $paginator = $blogRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/blogs/index.html.twig', [
            'title' => 'Блоги',
            'files_dir' => 'blogs',
            'blogs' => $paginator 
        ] + $paginateHelper -> getViewInfo($blogRepository->getNum(), 10)
       );
    }

    #[Route('/admin/blogs/form', methods:["GET"], name:'blog_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/blogs/form_new.html.twig', [
            'title' => 'Добавить новый блог',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/blogs', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        try
        {
            if( !( $request->request->has('title') && 
                $request->request->has('description') && 
                $request->request->has('text')))
            {
                throw new BadRequestHttpException("Не заполнены необходимые поля");
            }

            
            $blog = new Blog();
            $blog->setTitle($request->request->get('title'));
            $blog->genCanonicalUrl();
            $blog->setCreatedAt(new \DateTimeImmutable());
            $blog->setDescription($request->request->get('description'));
            $blog->setText($request->request->get('text'));
            $doctrine->getRepository(Blog::class)->save($blog, true);
            
            return $this->redirectToRoute('admin_blogs'); 

        } catch(BadRequestHttpException $err)
        {

            return $this->redirectToRoute(
                'blog_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/blogs/{id}/{slug}', methods:["GET"], name:'blog_form_update')]
    public function updateForm(int $id, string $slug, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Blog::class);
        $blog = $repository->find($id);

        if(!$blog)
        { 
            throw $this->createNotFoundException( 'Нет блога с id = '.$id.'!' );
        } else if( $blog->getCanonicalUrl() !== $slug)
        {
            return $this->redirectToRoute('blog_form_update', ['id'=>$id, 'slug'=> $blog->getCanonicalUrl()], Response::HTTP_MOVED_PERMANENTLY);
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/blogs/form_update.html.twig', [
            'title' => 'Блог: "'.$blog->getTitle().'"',
            'blog' => $blog,
            'files_dir' => 'blogs',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/blogs/{id}', methods:["PUT", "POST"])]
    public function updateBlog(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('title') && 
                    $input->has('canonical-url') && 
                    $input->has('description') && 
                    $input->has('text')))
            {
                throw new BadRequestHttpException("Не заполнены обязательные поля");
            }

            $blog = $entityManager->getRepository(Blog::class)->find($id);

            if(!$blog)
            { 
                throw $this->createNotFoundException(
                    'Нет блога с id = '.$id.'!'
                );
            }

            $blog->setTitle($input->get('title'));
            $blog->setCanonicalUrl($input->get('canonical-url'));
            $blog->setDescription($input->get('description'));
            $blog->setText($input->get('text'));
            $entityManager->flush();



        } catch(BadRequestHttpException  $err_text)
        {
            return $this->redirectToRoute(
                'blog_form_update',
                ['id' => $id, 
                 'slug' => $blog->getCanonicalUrl(), 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'blog_form_update',
            ['id' => $id, 
             'slug' => $blog->getCanonicalUrl(), 
             'success' => 1, 
             'err' => 0]
        );
    }




   #[Route('/admin/blogs/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(Blog::class);
       $blog = $repository->find($id);

       if(!$blog)
       { 
           throw $this->createNotFoundException( 'Нет блога с id = '.$id.'!' );
       }

       $repository->remove($blog, true);

       return new JsonResponse(["ok"]);

   }

}