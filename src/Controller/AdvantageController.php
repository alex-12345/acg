<?php

namespace App\Controller;

use App\Entity\Advantage;
use App\Utils\ImgUploader;
use App\Utils\PaginateHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class AdvantageController extends AbstractController
{
    #[Route('/admin/advantages', name: 'admin_advantages', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $advantageRepository = $doctrine->getRepository(Advantage::class);
        $paginator = $advantageRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/advantages/index.html.twig', [
            'title' => 'Преимущества',
            'files_dir' => 'advantages',
            'advantages' => $paginator 
        ] + $paginateHelper -> getViewInfo($advantageRepository->getNum(), 10)
       );

    }

    #[Route('/admin/advantages/form', methods:["GET"], name:'advantage_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/advantages/form_new.html.twig', [
            'title' => 'Добавить новое преимущество',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/advantages', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        $input = $request->request;
        $file = $request->files;

        try
        {
            if( !( $input->has('title') && 
                   $input->has('text') && 
                   $file->has('file') && 
                   !is_null($file->get('file'))))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $advantage = new Advantage();
            $advantage->setTitle($input->get('title'));
            $advantage->setText($input->get('text'));
            $uploader = new ImgUploader($file->get('file'), 'files/advantages', ImgUploader::$JPEG_PNG_MIME);
            $uploader->loadImg(300, 300, 1.2, 0.8);
            $uploader->saveImg([100, 200, 300]);

            $advantage->setImg($uploader->getPath());

            $doctrine->getRepository(Advantage::class)->save($advantage, true);

            return $this->redirectToRoute('admin_advantages'); 

        } catch( BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'advantage_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/advantages/{id}', methods:["GET"], name:'advantage_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Advantage::class);
        $advantage = $repository->find($id);
        
        if(!$advantage)
        { 
            throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/advantages/form_update.html.twig', [
            'title' => 'Преимущество: "'.$advantage->getTitle().'"',
            'advantage' => $advantage,
            'files_dir' => 'advantages',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }


    #[Route('/admin/advantages/{id}', methods:["PUT", "POST"])]
    public function updateAdvantage(int $id,  Request $request, ManagerRegistry $doctrine, CacheInterface $cache): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('title') && 
                   $input->has('text')))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $advantage = $entityManager->getRepository(Advantage::class)->find($id);

            if(!$advantage)
            { 
                throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
            }

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/advantages', ImgUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(300, 300, 1.2, 0.8);
                $uploader->saveImg([100, 200, 300]);

                $advantage->setImg($uploader->getPath());
            }
            
            $advantage->setTitle($input->get('title'));
            $advantage->setText($input->get('text'));

            $entityManager->flush();
            $cache->delete($entityManager->getRepository(Advantage::class)::$CACHE_NAME);



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'advantage_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'advantage_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }

   #[Route('/admin/advantages/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(Advantage::class);
       $advantage = $repository->find($id);

       if(!$advantage)
       { 
           throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
       }

       $repository->remove($advantage, true);

       return new JsonResponse(["ok"]);

   }
    

    

}