<?php

namespace App\Controller;

use App\Entity\Service;
use App\Entity\ServiceItem;
use App\Utils\PaginateHelper;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ServiceItemController extends AbstractController
{
    #[Route('/admin/service_items', name: 'admin_service_items', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $serviceItemRepository = $doctrine->getRepository(ServiceItem::class);
        $paginator = $serviceItemRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/service_items/index.html.twig', [
            'title' => 'Услуги',
            'service_items' => $paginator 
        ] + $paginateHelper -> getViewInfo($serviceItemRepository->getNum(), 10)
       );
    }

    #[Route('/admin/service_items/form', methods:["GET"], name:'service_item_form_new')]
    public function addForm(Request $request, ManagerRegistry $doctrine): Response
    { 
        $query_params = $request->query;

        $serviceRepository = $doctrine->getRepository(Service::class);
        $select_block = $serviceRepository->findAll();
        usort($select_block, fn ($a, $b) => strcmp($a->getName(), $b->getName()));

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/service_items/form_new.html.twig', [
            'title' => 'Добавить новую услугу',
            'select_block' => $select_block,
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/service_items', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        try
        {
            if( !( $request->request->has('name') && 
                $request->request->has('description') && 
                $request->request->has('service') && 
                intval($request->request->has('service')) &&
                $request->request->has('price')))
            {
                throw new BadRequestHttpException("Не заполнены необходимые поля");
            }

            $service = $doctrine->getRepository(Service::class)->find(intval($request->request->get('service')));

            if (is_null($service))
            {
                throw new BadRequestHttpException("Передан неправильный блок услуг");
            }
            $serviceItem = new ServiceItem();
            $serviceItem->setName($request->request->get('name'));
            $serviceItem->setDescription($request->request->get('description'));
            $serviceItem->setService($service);
            $serviceItem->setPrice($request->request->get('price'));
            $doctrine->getRepository(ServiceItem::class)->save($serviceItem, true);
            
            return $this->redirectToRoute('admin_service_items'); 

        } catch(BadRequestHttpException $err)
        {

            return $this->redirectToRoute(
                'service_item_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/service_items/{id}', methods:["GET"], name:'service_item_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(ServiceItem::class);
        $serviceItem = $repository->find($id);

        if(!$serviceItem)
        { 
            throw $this->createNotFoundException( 'Нет услуги с id = '.$id.'!' );
        }

        $serviceRepository = $doctrine->getRepository(Service::class);
        $select_block = $serviceRepository->findAllExcludeOne($serviceItem->getService());
        usort($select_block, fn ($a, $b) => strcmp($a->getName(), $b->getName()));

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/service_items/form_update.html.twig', [
            'title' => 'Услуга: "'.$serviceItem->getName().'"',
            'service_item' => $serviceItem,
            'files_dir' => 'service_items',
            'select_block' => $select_block,
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/service_items/{id}', methods:["PUT", "POST"])]
    public function updateServiceItem(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $request->request->has('name') && 
                $request->request->has('description') && 
                $request->request->has('service') && 
                intval($request->request->has('service')) &&
                $request->request->has('price')))
            {
                throw new BadRequestHttpException("Не заполнены обязательные поля");
            }

            $service = $doctrine->getRepository(Service::class)->find(intval($request->request->get('service')));
            $serviceItem = $entityManager->getRepository(ServiceItem::class)->find($id);

            if (is_null($service) ||is_null($serviceItem) ) 
            {
                throw new BadRequestHttpException("Передан несуществующий объекут");
            }

            $serviceItem->setName($request->request->get('name'));
            $serviceItem->setDescription($request->request->get('description'));
            $serviceItem->setService($service);
            $serviceItem->setPrice($request->request->get('price'));
            $entityManager->flush();

        } catch(BadRequestHttpException  $err_text)
        {
            return $this->redirectToRoute(
                'service_item_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'service_item_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }




   #[Route('/admin/service_items/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(ServiceItem::class);
       $serviceItem = $repository->find($id);

       if(!$serviceItem)
       { 
           throw $this->createNotFoundException( 'Нет услуги с id = '.$id.'!' );
       }

       try
       {
            $repository->remove($serviceItem, true);
            return new JsonResponse(["ok"]);

       } catch (ForeignKeyConstraintViolationException $err)
       {
            return new JsonResponse(["Ошибка, от данной услуги зависят другие объекты - удалите сначала их!"], JsonResponse::HTTP_CONFLICT);
       }
   }

}