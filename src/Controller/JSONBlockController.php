<?php

namespace App\Controller;

use App\Entity\JSONBlock;
use App\Utils\PaginateHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class JSONBlockController extends AbstractController
{
    #[Route('/admin/json_blocks', name: 'admin_json_blocks', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,12);

        $JSONBlockRepository = $doctrine->getRepository(JSONBlock::class);
        $paginator = $JSONBlockRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/json_blocks/index.html.twig', [
            'title' => 'JSON блоки',
            'files_dir' => 'json_blocks',
            'json_blocks' => $paginator 
        ] + $paginateHelper -> getViewInfo($JSONBlockRepository->getNum(), 10)
       );
    }

    #[Route('/admin/json_blocks/repair', methods:["POST", "GET"])]
    public function repair(ManagerRegistry $doctrine): Response
    { 
        $repository = $doctrine->getRepository(JSONBlock::class);
        $repository->repair();
       

        return new JsonResponse(['ok']); 

        
    }

    #[Route('/admin/json_blocks/{id}', methods:["GET"], name:'json_block_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(JSONBlock::class);
        $JSONBlock = $repository->find($id);

        if(!$JSONBlock)
        { 
            throw $this->createNotFoundException( 'Нет JSON блока с id = '.$id.'!' );
        } 

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/json_blocks/form_update.html.twig', [
            'title' => 'JSON блок: "'.$JSONBlock->getKey().'"',
            'json_block' => $JSONBlock,
            'files_dir' => 'json_blocks',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/json_blocks/{id}', methods:["PUT", "POST"])]
    public function updatejson_block(int $id,  Request $request, ManagerRegistry $doctrine, CacheInterface $cache): Response
    {
        $input = $request->request;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('key') && 
                   $input->has('description')&& 
                   $input->has('data')))
            {
                throw new BadRequestHttpException("Не заполнены обязательные поля");
            }

            $JSONBlock = $entityManager->getRepository(JSONBlock::class)->find($id);

            $data = $request->request->get('data');

            if(!(is_string($data) && is_array(json_decode($data, true)) && (json_last_error() == JSON_ERROR_NONE)))
            {
                throw new BadRequestHttpException("Передан некорректный json");

            }
            $data = json_encode(json_decode($data), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

            if(!$JSONBlock)
            { 
                throw $this->createNotFoundException(
                    'Нет JSON блока с id = '.$id.'!'
                );
            }

            $JSONBlock->setKey($input->get('key'));
            $JSONBlock->setDescription($input->get('description'));
            $JSONBlock->setData($data);
            $entityManager->flush();
            $cache->delete($entityManager->getRepository(JSONBlock::class)::$CACHE_NAME);
            



        } catch(BadRequestHttpException  $err_text)
        {
            return $this->redirectToRoute(
                'json_block_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'json_block_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }

}