<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'app_login', methods: ["GET", "POST"])]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
         $error = $authenticationUtils->getLastAuthenticationError();
         $lastUsername = $authenticationUtils->getLastUsername();

          return $this->render('admin/login/index.html.twig', [
            'title' => 'Вход в админ панель',
            'last_username' => $lastUsername,
            'error'         => $error,
          ]);
    }

    #[Route('/admin/logout', name: 'app_logout', methods: ["GET"])]
    public function logout()
    { }
}
