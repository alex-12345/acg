<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Entity\SubscriptionService;
use App\Utils\ImgUploader;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use App\Utils\ServiceType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{

    #[Route('/services/{type}', name: 'services_index', methods:["GET"])]
    public function index(string $type, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        
        if ( ! ServiceType::tryFrom($type ))
        {
            throw $this->createNotFoundException();
        }

        $json_blocks = $jsonBlockLoader->load($doctrine);

        $uri = "services/".$type;

        $serviceRepository = $doctrine->getRepository(Service::class);

        $services = $serviceRepository->findAllByType(ServiceType::tryFrom($type ));
        
        return $this->render('user/services/index.html.twig', [

            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, $uri, "article") ,
            'json_blocks' => $json_blocks,
            'titles' => $json_blocks["titles"][$uri],
            'uri' => $uri,
            'services' => $services,
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20)
        ]
       );
    }


    #[Route('/services/{type}/{slug}', name: 'service_view', methods:["GET"])]
    public function view(string $type, string $slug, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $repository = $doctrine->getRepository(Service::class);
        $service = $repository->findOneByCanonicalUrl($slug);

        if(!$service)
        { 
            throw $this->createNotFoundException( );
        } else if( $service->getType() !== ServiceType::tryFrom($type))
        {
            return $this->redirectToRoute('service_view', ['type'=>$service->getType()->value, 'slug'=> $service->getCanonicalUrl()], Response::HTTP_MOVED_PERMANENTLY);
        }

        $json_blocks = $jsonBlockLoader->load($doctrine);

        $uri = "services/".$type;

        $description = "Блок услуг \"" .$service->getName() . "\": ";

        foreach ($service->getServiceItems() as $item)
        {
            $tmp = mb_substr($item->getName(), 0, 50);
            $description .= $tmp . ", ";
        }

        $description = mb_substr($description, 0, -2);

        return $this->render('user/services/view.html.twig', [
            'json_blocks' => $json_blocks,
            'titles' => $json_blocks["titles"][$uri],
            'uri' => $uri,
            'service' => $service,
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'seo' => SEOHelper::getSEOFromData($service->getName(), $description, $uri."/".$service->getCanonicalUrl(), "article"),
        ]
       );
    }

    #[Route('/admin/services', name: 'admin_services', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,12);

        $serviceRepository = $doctrine->getRepository(Service::class);
        $paginator = $serviceRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/services/index.html.twig', [
            'title' => 'Блоки услуг',
            'files_dir' => 'services',
            'services' => $paginator 
        ] + $paginateHelper -> getViewInfo($serviceRepository->getNum(), 10)
       );
    }

    #[Route('/admin/services/form', methods:["GET"], name:'service_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/services/form_new.html.twig', [
            'title' => 'Добавить новый блок услуг',
            'select_block' => ServiceType::cases(),
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/services', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        $input = $request->request;
        $file = $request->files;
        try
        {
            if( !( $input->has('name') && 
                    $input->has('type')&&  
                    ServiceType::tryFrom($input->get('type'))))
            {
                throw new BadRequestHttpException("Не заполнены необходимые поля");
            }

            $service = new Service();
            $service->setName($input->get('name'));
            $service->setType(ServiceType::from($input->get('type')));
            $service->genCanonicalUrl();

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/services', ImgUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(50, 50, 1.2, 0.8);
                $uploader->saveImg([50, 65]);

                $service->setImg($uploader->getPath());
               
            };
            
            $doctrine->getRepository(Service::class)->save($service, true);
            return $this->redirectToRoute('admin_services'); 

        } catch(BadRequestHttpException | FileException $err)
        {
            return $this->redirectToRoute(
                'service_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        } catch (UniqueConstraintViolationException $err)
        {
            return $this->redirectToRoute(
                'service_form_new',
                ['err' => 1, 'error_text' => "Имя блока услуг не уникально"]
            );
        }
    }

    #[Route('/admin/services/{type}/{slug}', methods:["GET"], name:'service_form_update')]
    public function updateForm(string $type, string $slug, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Service::class);
        $service = $repository->findOneByCanonicalUrl($slug);

        if(!$service)
        { 
            throw $this->createNotFoundException( 'Нет блока услуг с canonical url = '.$slug.'!' );
        } else if( $service->getType() !== ServiceType::tryFrom($type))
        {
            return $this->redirectToRoute('service_form_update', ['type'=>$service->getType()->value, 'slug'=> $service->getCanonicalUrl()], Response::HTTP_MOVED_PERMANENTLY);
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        $select_block = ServiceType::cases();

        unset($select_block[array_search($service->getType(), $select_block)]);

        return $this->render('admin/services/form_update.html.twig', [
            'title' => 'Блок услуг: "'.$service->getName().'"',
            'service' => $service,
            'select_block' => $select_block,
            'files_dir' => 'services',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/services/{type}/{slug}', methods:["PUT", "POST"])]
    public function updateService(string $type, string $slug, Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('name') && 
                   $input->has('canonical-url')&& 
                   $input->has('type'))&&  
                   ServiceType::tryFrom($input->get('type')))
            {
                throw new BadRequestHttpException("Не заполнены обязательные поля");
            }

            $service = $entityManager->getRepository(Service::class)->findOneByCanonicalUrl($slug);

            if(!$service)
            { 
                throw $this->createNotFoundException( 'Нет блока услуг с canonical url = '.$slug.'!' );
            } else if( $service->getType() !== ServiceType::tryFrom($type))
            {
                return $this->redirectToRoute('service_form_update', ['type'=>$service->getType()->value, 'slug'=> $service->getCanonicalUrl()], Response::HTTP_MOVED_PERMANENTLY);
            }

            $service->setName($input->get('name'));
            $service->setType(ServiceType::from($input->get('type')));
            $service->setCanonicalUrl($input->get('canonical-url'));


            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/services', ImgUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(100, 100, 1.2, 0.8);
                $uploader->saveImg([50, 65, 100]);

                $service->setImg($uploader->getPath());
               
            };

            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'service_form_update',
                ['type' => $type,
                 'slug' => $slug, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        } catch (UniqueConstraintViolationException $err)
        {
            return $this->redirectToRoute(
                'service_form_update',
                ['err' => 1,
                 'type' => $type,
                 'slug' => $slug, 
                 'error_text' => "Имя блока услуг или его URL не уникально"]
            );
        }

        return $this->redirectToRoute(
            'service_form_update',
            ['slug' => $service->getCanonicalUrl(), 
             'type' => $service->getType()->value, 
             'success' => 1, 
             'err' => 0]
        );
    }

   #[Route('/admin/services/{type}/{slug}', methods:["DELETE"])]
   public function remove(string $type, string $slug, ManagerRegistry $doctrine): Response
   {
        $repository = $doctrine->getRepository(Service::class);
        $service = $repository->findOneByCanonicalUrl($slug);

        if(!$service)
        { 
            throw $this->createNotFoundException( 'Нет блока услуг с canonical url = '.$slug.'!' );
        }
        try
        {
            $repository->remove($service, true);
            return new JsonResponse(["ok"]);

        } catch (ForeignKeyConstraintViolationException $err)
        {
                return new JsonResponse(["Ошибка, от данного блока зависят другие объекты - удалите сначала их!"], JsonResponse::HTTP_CONFLICT);
        }


   }

}