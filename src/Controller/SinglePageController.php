<?php

namespace App\Controller;

use App\Entity\AboutItem;
use App\Entity\Advantage;
use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Entity\SubscriptionService;
use App\Utils\JsonBlockLoader;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SinglePageController extends AbstractController
{
    #[Route('/', name: 'main_page', methods:["GET"])]
    public function main(ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $json_blocks = $jsonBlockLoader->load($doctrine);
        
        return $this->render('user/single_pages/main.html.twig', [
            'title' => 'Услуги',
            'json_blocks' => $json_blocks,
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "", "website") ,
            'service_blocks' => $doctrine->getRepository(Service::class)->findAll(),
            'advantages_banner' => $doctrine->getRepository(Advantage::class)->findAll(),
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3)
        ] 
       );
    }

    #[Route('/about', name: 'about_page', methods:["GET"])]
    public function about(ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        
        $json_blocks = $jsonBlockLoader->load($doctrine);

        $aboutItemRepository = $doctrine->getRepository(AboutItem::class);
        $aboutItems = $aboutItemRepository->findAll();
        
        return $this->render('user/single_pages/about.html.twig', [
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "about", "article") ,
            'advantages_banner' => $doctrine->getRepository(Advantage::class)->findAll(),
            'json_blocks' => $json_blocks,
            'about_items' => array_reverse($aboutItems),
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3)
        ]
       );
    }

    #[Route('/prices', name: 'prices_page', methods:["GET"])]
    public function prices(ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        
        $json_blocks = $jsonBlockLoader->load($doctrine);

        $serviceRepository = $doctrine->getRepository(Service::class);
        $prices = $serviceRepository->findAll();

        return $this->render('user/single_pages/prices.html.twig', [
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "prices", "article") ,
            'json_blocks' => $json_blocks,
            'prices' => array_reverse($prices),
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3)
        ]
       );
    }


    #[Route('/contacts', name: 'contacts_page', methods:["GET"])]
    public function contacts(ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $json_blocks = $jsonBlockLoader->load($doctrine);
        
        return $this->render('user/single_pages/contacts.html.twig', [
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "contacts", "article") ,
            'json_blocks' => $json_blocks,
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3)
        ]
       );
    }
    public function error(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response

    {
        $json_blocks = $jsonBlockLoader->load($doctrine);
        $redirected_pages = $json_blocks["redirected"];
        $cur_path = $request->getPathInfo();

        if (array_key_exists($cur_path, $redirected_pages))
        {
            return $this->redirect($redirected_pages[$cur_path]);
        }
        return  $this->render('user/single_pages/error404.html.twig', [
            'seo' => ['title' => "404 Not Found", "description" => "Запрашиваемая страница не найдена!", "type" => "noindex"],
            'json_blocks' => $json_blocks
        ], new Response('',404));
    }

}