<?php

namespace App\Controller;

use App\Entity\Doc;
use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Utils\FileUploader;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DocController extends AbstractController
{

    #[Route('/docs', methods:["GET"])]
    public function index(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request, 12);

        $docRepository = $doctrine->getRepository(Doc::class);
        $paginator = $docRepository->getListPaginator($paginateHelper);
        
        $json_blocks = $jsonBlockLoader->load($doctrine);
        
        return $this->render('user/single_pages/docs.html.twig', [
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "docs", "article") ,
            'docs' => $paginator,
            'json_blocks' => $json_blocks ,
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'files_dir' => 'docs'
        ] + $paginateHelper -> getViewInfo($docRepository->getNum(), 8) );
    }


    #[Route('/admin/docs', name: 'admin_docs', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,12);

        $docRepository = $doctrine->getRepository(Doc::class);
        $paginator = $docRepository->getListPaginator($paginateHelper);
        
        return $this->render( 'admin/docs/index.html.twig', [
            'title' => 'Документы',
            'files_dir' => 'docs',
            'docs' => $paginator 
        ] + $paginateHelper -> getViewInfo($docRepository->getNum(), 10) );
    }

    #[Route('/admin/docs/form', methods:["GET"], name:'doc_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/docs/form_new.html.twig', [
            'title' => 'Добавить новый документ',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/docs', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        try
        {
            if( !( $request->request->has('name') && 
                $request->request->has('description') && 
                $request->files->has('file')))
            {
                throw new BadRequestHttpException("Нет файла, его названия или описания");
            }

            $file = $request->files->get('file');
            $uploader = new FileUploader($file, 'files/docs', FileUploader::$ZIP_PDF_MIME);

            $uploader->checkMimeAndSize();
            $url = $uploader->upload();
            
            $doc = new Doc();
            $doc->setName($request->request->get('name'));
            $doc->setUrl($url);
            $doc->setCreatedAt(new \DateTimeImmutable());
            $doc->setDescription($request->request->get('description'));
            $doctrine->getRepository(Doc::class)->save($doc, true);
            
            return $this->redirectToRoute('admin_docs'); 

        } catch(BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'doc_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }


    #[Route('/admin/docs/{id}', methods:["GET"], name:'doc_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Doc::class);
        $doc = $repository->find($id);
        
        if(!$doc)
        { 
            throw $this->createNotFoundException( 'Нет документа с id = '.$id.'!' );
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/docs/form_update.html.twig', [
            'title' => 'Документ: "'.$doc->getName().'"',
            'doc' => $doc,
            'files_dir' => 'docs',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/docs/{id}', methods:["PUT", "POST"])]
    public function updateDoc(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('name') && 
                $input->has('description') ))
            {
                throw new BadRequestHttpException("Не заполнены обязательные поля");
            }

            $doc = $entityManager->getRepository(Doc::class)->find($id);

            if(!$doc)
            { 
                throw $this->createNotFoundException(
                    'Нет документа с id = '.$id.'!'
                );
            }

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new FileUploader($file, 'files/docs', FileUploader::$ZIP_PDF_MIME);
                $uploader->checkMimeAndSize();
                $url = $uploader->upload();
                
                $doc->setUrl($url);
            }

            $doc->setName($input->get('name'));
            $doc->setDescription($input->get('description'));
            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'doc_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'doc_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }

    #[Route('/admin/docs/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(Doc::class);
       $doc = $repository->find($id);

       if(!$doc)
       { 
           throw $this->createNotFoundException( 'Нет документа с id = '.$id.'!' );
       }

       $repository->remove($doc, true);

       return new JsonResponse(["ok"]);

   }


}
