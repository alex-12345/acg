<?php

namespace App\Controller;

use App\Entity\Request as ClientRequest;
use App\Events\NewRequestEvent;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RequestController extends AbstractController
{

    #[Route('/requests', methods:["POST", "GET"])]
    public function add(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader, EventDispatcherInterface $dispatcher): Response
    {
        $json_blocks =  $jsonBlockLoader->load($doctrine);
        
        $input = $request->request;
        // $input = $request->query;

        try{
            if( !( $input->has('name') && 
                $input->has('phone') && $input->has('token')))
            {
                throw new BadRequestHttpException();
            }

            $title  = "Успешно отправлено";

            $name = $input->get('name');
            $phone = $input->get('phone');
            $token = $input->get('token');

            if (mb_strlen($name) < 1 || mb_strlen($name) > 50 || !preg_match("/^\+7 \([0-9]{3}\) [0-9]{3} [0-9]{4}$/i",$phone) || !$this->isCsrfTokenValid('add-request', $token))
            {
                throw new BadRequestHttpException();
            }

            $text = '';
            if ($input->has('url_src') && filter_var($input->get('url_src'), FILTER_VALIDATE_URL) )
            {
                $text = "Заявка осуществлена со страницы ". $input->get('url_src');
            } else
            {
                $text = "Не удалось определить с какой страницы была осуществлена заявка";
            }

            $clientRequest = new ClientRequest();
            $clientRequest->setClientName($name);
            $clientRequest->setPhone($phone);
            $clientRequest->setText($text);
            $clientRequest->setCreatedAt((new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Moscow')));
            $requestRepository = $doctrine->getRepository(ClientRequest::class);
            $requestRepository->save($clientRequest, true);

            $dispatcher->dispatch(new NewRequestEvent($clientRequest));



        } catch(BadRequestHttpException $err)
        {
            $title  = "Ошибка";

            return $this->render('user/requests/error.html.twig', [
                'json_blocks' => $json_blocks,
                'title' => $title,
                'sub_title' => "Форма заполнена некорректно",
                'seo' => SEOHelper::getSEOFromData($title, "", "requests", "article"),
            ]);
        }

        return $this->render('user/requests/success.html.twig', [
            'json_blocks' => $json_blocks,
            'title' => $title,
            'sub_title' => "Мы позвоним вам в ближайшее время",
            'seo' => SEOHelper::getSEOFromData($title, "", "requests", "article"),
            'name' => $name,
            'phone' => $phone
        ]);
    }


    #[Route('/admin/requests', name: 'admin_requests', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,20);

        $requestRepository = $doctrine->getRepository(ClientRequest::class);
        $paginator = $requestRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/requests/index.html.twig', [
            'title' => 'Заявки клиентов',
            'requests' => $paginator 
        ] + $paginateHelper -> getViewInfo($requestRepository->getNum(), 10)
       );
    }
}