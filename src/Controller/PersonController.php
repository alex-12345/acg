<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Utils\FileUploader;
use App\Utils\ImgUploader;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{

    #[Route('/persons', name: 'persons', methods:["GET"])]
    public function index(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request, 8);

        $repository = $doctrine->getRepository(Person::class);
        $paginator = $repository->getListPaginator($paginateHelper);

        $json_blocks =  $jsonBlockLoader->load($doctrine);

        return $this->render('user/persons/index.html.twig', [
            'json_blocks' => $json_blocks,
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "persons", "article"),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20) ,
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons' => $paginator 
        ] + $paginateHelper -> getViewInfo($repository->getNum(), 10)
       );
    }

    #[Route('/persons/{id}', name: 'person_view', methods:["GET"])]
    public function view(int $id, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $repository = $doctrine->getRepository(Person::class);
        $person = $repository->find($id);

        if(!$person)
        { 
            throw $this->createNotFoundException( );
        } 
        
        $json_blocks = $jsonBlockLoader->load($doctrine);

        $uri = "persons/".$id;

        $description = mb_substr($person->getBriefInfo(), 0, 350); 

        return $this->render('user/persons/view.html.twig', [
            'json_blocks' => $json_blocks,
            'person' => $person,
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'seo' => SEOHelper::getSEOFromData($person->getFullName(), $description, "persons/".$id, "article"),
        ]
       );
    }

    #[Route('/admin/persons', name: 'admin_persons', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $personRepository = $doctrine->getRepository(Person::class);
        $paginator = $personRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/persons/index.html.twig', [
            'title' => 'Сотрудники',
            'files_dir' => 'persons',
            'persons' => $paginator 
        ] + $paginateHelper -> getViewInfo($personRepository->getNum(), 10)
       );
    }

    #[Route('/admin/persons/form', methods:["GET"], name:'person_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/persons/form_new.html.twig', [
            'title' => 'Добавить нового сотрудника',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/persons', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;
        try
        {
            if( !( $input->has('full-name') && 
                $input->has('position') && 
                $input->has('brief-info') && 
                $input->has('rank') &&
                $input->has('qualification')&& 
                $input->has('file-start-x')&& 
                $input->has('file-start-y')&& 
                $input->has('file-width') && 
                $input->has('file-height') && 
                $file->has('file') && 
                !is_null($file->get('file')) ))
            {
                throw new BadRequestHttpException("Не переданны обязательные данные!");
            }

            $img = $file->get('file');
            $uploader = new ImgUploader($img, 'files/persons', FileUploader::$JPEG_PNG_MIME);
            $uploader->loadImg(550, 700, 0.80, 0.75);
            $uploader->saveImg([450], ['sizes'=> [100, 160,320], 
                                       'x' => $input->get('file-start-x'),  
                                       'y' => $input->get('file-start-y'),  
                                       'width' => $input->get('file-width'),  
                                       'height' => $input->get('file-height')]);
            
            $person = new Person();
            $person->setFullName($input->get('full-name'));
            $person->setImg($uploader->getPath());
            $person->setRank(intval($input->get('rank')));
            $person->setPosition($input->get('position'));
            $person->setBriefInfo($input->get('brief-info'));
            $person->setQualification($input->get('qualification'));

            $doctrine->getRepository(Person::class)->save($person, true);

            return $this->redirectToRoute('admin_persons'); 

        } catch( BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'person_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/persons/{id}', methods:["GET"], name:'person_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Person::class);
        $person = $repository->find($id);
        
        if(!$person)
        { 
            throw $this->createNotFoundException( 'Нет сотрудника с id = '.$id.'!' );
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/persons/form_update.html.twig', [
            'title' => 'Сотрудник: "'.$person->getFullName().'"',
            'person' => $person,
            'files_dir' => 'persons',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/persons/{id}', methods:["PUT", "POST"])]
    public function updatePerson(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();


        try
        {
            if( !( $input->has('full-name') && 
                $input->has('position') && 
                $input->has('brief-info') && 
                $input->has('rank') &&
                $input->has('qualification')))
            {
                throw new BadRequestHttpException("Не переданны обязательные данные!");
            }
            

            $person = $entityManager->getRepository(Person::class)->find($id);

            if(!$person)
            { 
                throw $this->createNotFoundException( 'Нет сотрудника с id = '.$id.'!' );
            }

            if( $file->has('file')  &&
                !is_null($file->get('file')) &&
                $input->has('file-start-x')&& 
                $input->has('file-start-y')&& 
                $input->has('file-width') && 
                $input->has('file-height') 
            
              )
            {
                $img = $file->get('file');
                $uploader = new ImgUploader($img, 'files/persons', FileUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(550, 700, 0.80, 0.75);
                $uploader->saveImg([450], ['sizes'=> [100, 160,320], 
                                        'x' => $input->get('file-start-x'),  
                                        'y' => $input->get('file-start-y'),  
                                        'width' => $input->get('file-width'),  
                                        'height' => $input->get('file-height')]);

                $person->setImg($uploader->getPath());
            }
            

            $person->setFullName($input->get('full-name'));
            $person->setPosition($input->get('position'));
            $person->setBriefInfo($input->get('brief-info'));
            $person->setRank(intval($input->get('rank')));
            $person->setQualification($input->get('qualification'));


            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'person_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'person_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }



    #[Route('/admin/persons/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(Person::class);
       $person = $repository->find($id);

       if(!$person)
       { 
           throw $this->createNotFoundException( 'Нет сотрудника с id = '.$id.'!' );
       }

       $repository->remove($person, true);

       return new JsonResponse(["ok"]);

   }
}
