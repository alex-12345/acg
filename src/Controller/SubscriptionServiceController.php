<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Entity\ServiceItem;
use App\Entity\ServiceItemSubscriptionService;
use App\Entity\SubscriptionService;
use App\Utils\ImgUploader;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionServiceController extends AbstractController
{
    #[Route('/subscription_services', name: 'subscription_services', methods:["GET"])]
    public function index(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $json_blocks =  $jsonBlockLoader->load($doctrine);
        
        return $this->render('user/subscription_services/index.html.twig', [
            'json_blocks' => $json_blocks,
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "subscription_services", "article"),
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20) ,
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'subscription_services' => $doctrine->getRepository(SubscriptionService::class)->findAll() 
        ]
       );

    }

    #[Route('/subscription_services/{id}', name: 'subscription_services_view', methods:["GET"])]
    public function view(int $id, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $json_blocks =  $jsonBlockLoader->load($doctrine);

        $repository = $doctrine->getRepository(SubscriptionService::class);
        $subscriptionService = $repository->find($id);
        
        if(!$subscriptionService)
        { 
            throw $this->createNotFoundException(  );
        }

        $serviceItems = \iterator_to_array($subscriptionService->getServiceItemSubscriptionServices());
        usort($serviceItems, fn ($a, $b) => intval($a->getId() > $b->getId()));

        $description = "Пакет абонентского обслуживания \"" .$subscriptionService->getName() . "\". ". mb_substr($subscriptionService->getDescription(), 0, 160);

        
        return $this->render('user/subscription_services/view.html.twig', [
            'json_blocks' => $json_blocks,
            'subscription_service' => $subscriptionService,
            'service_items' => $serviceItems,
            'reviews_banner' => $doctrine->getRepository(Review::class)->findRandom(20),
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'seo' => SEOHelper::getSEOFromData(
                $subscriptionService->getName() . " - ". "пакет абонентского обслуживания", 
                $description, 
                "subscription_services/".$subscriptionService->getId(), 
                "article",
                "files/subscription_services/200_w/". $subscriptionService->getImg()),
        ]
       );

    }


    #[Route('/admin/subscription_services', name: 'admin_subscription_services', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $subscriptionServiceRepository = $doctrine->getRepository(SubscriptionService::class);
        $paginator = $subscriptionServiceRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/subscription_services/index.html.twig', [
            'title' => 'Абонентское обслуживание',
            'files_dir' => 'subscription_services',
            'subscription_services' => $paginator 
        ] + $paginateHelper -> getViewInfo($subscriptionServiceRepository->getNum(), 10)
       );

    }

    #[Route('/admin/subscription_services/form', methods:["GET"], name:'subscription_service_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/subscription_services/form_new.html.twig', [
            'title' => 'Добавить новый пакет абонентского обслуживания',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/subscription_services', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        $input = $request->request;
        $file = $request->files;

        try
        {
            if( !( $input->has('name') && 
                   $input->has('sub_title') && 
                   $input->has('description') && 
                   $input->has('price') && 
                   $file->has('file') && 
                   !is_null($file->get('file'))))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $subscriptionService = new SubscriptionService();
            $subscriptionService->setName($input->get('name'));
            $subscriptionService->setSubTitle($input->get('sub_title'));
            $subscriptionService->setDescription($input->get('description'));
            $subscriptionService->setPrice($input->get('price'));
            $uploader = new ImgUploader($file->get('file'), 'files/subscription_services', ImgUploader::$JPEG_PNG_MIME);
            $uploader->loadImg(560, 460, 1.4, 0.6);
            $uploader->saveImg([100, 200, 560]);

            $subscriptionService->setImg($uploader->getPath());

            $doctrine->getRepository(SubscriptionService::class)->save($subscriptionService, true);

            return $this->redirectToRoute('admin_subscription_services'); 

        } catch( BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'subscription_service_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/subscription_services/{id}', methods:["GET"], name:'subscription_service_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(SubscriptionService::class);
        $subscriptionService = $repository->find($id);
        
        if(!$subscriptionService)
        { 
            throw $this->createNotFoundException( 'Нет абонентского обслуживания с id = '.$id.'!' );
        }

        $serviceItems = \iterator_to_array($subscriptionService->getServiceItemSubscriptionServices());
        if( !count($serviceItems))
        {
            $otherItems = $doctrine->getRepository(ServiceItem::class)->findAll();

        } else
        {
            $ids = array_map( fn($x) : int => $x->getServiceItemMap()->getId() ,$serviceItems);
            $otherItems = $doctrine->getRepository(ServiceItem::class)->findByExludeIds($ids);
            usort($serviceItems, fn ($a, $b) => $a->getId() > $b->getId());
        }
        usort($otherItems, fn ($a, $b) => strcmp($a->getName(), $b->getName()));
        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $success_sub_id = ($query_params->has('success_sub_id'))? $query_params->get('success_sub_id'): 0;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/subscription_services/form_update.html.twig', [
            'title' => 'Пакет абонентского обслуживания: "'.$subscriptionService->getName().'"',
            'subscription_service' => $subscriptionService,
            'service_items' => $serviceItems,
            'other_items' => $otherItems,
            'files_dir' => 'subscription_services',
            'success_sub_id' => $success_sub_id,
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }


    #[Route('/admin/subscription_services/{id}', methods:["PUT", "POST"])]
    public function updateSubscriptionService(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('name') && 
                   $input->has('sub_title') && 
                   $input->has('description') && 
                   $input->has('price')))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $subscriptionService = $entityManager->getRepository(SubscriptionService::class)->find($id);

            if(!$subscriptionService)
            { 
                throw $this->createNotFoundException( 'Нет абонентского обслуживания с id = '.$id.'!' );
            }

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/subscription_services', ImgUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(560, 460, 1.4, 0.6);
                $uploader->saveImg([100, 200, 560]);

                $subscriptionService->setImg($uploader->getPath());
            }
            
            $subscriptionService->setName($input->get('name'));
            $subscriptionService->setDescription($input->get('description'));
            $subscriptionService->setPrice($input->get('price'));
            $subscriptionService->setSubTitle($input->get('sub_title'));

            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'subscription_service_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'subscription_service_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }

    #[Route('/admin/subscription_services/{id}/service_items', methods:["POST"])]
    public function addSubscriptionServiceServiceItem(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('service_item') && 
                   $input->has('amount') ))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $subscriptionService = $entityManager->getRepository(SubscriptionService::class)->find($id);
            $serviceItem = $entityManager->getRepository(ServiceItem::class)->find($input->get('service_item'));

            if(!$subscriptionService || !$serviceItem)
            { 
                throw $this->createNotFoundException( 'Услуга или абонентский пакет не существуют!' );
            }

            $serviceItemSubscriptionService = new ServiceItemSubscriptionService();
            $serviceItemSubscriptionService->setSubscriptionServiceMap($subscriptionService);
            $serviceItemSubscriptionService->setServiceItemMap($serviceItem);
            $serviceItemSubscriptionService->setAmount($input->get('amount'));

            $entityManager->getRepository(ServiceItemSubscriptionService::class)->save($serviceItemSubscriptionService, true);

            $id = $subscriptionService->getId();

        } catch(BadRequestHttpException $err_text)
        {
            return $this->redirectToRoute(
                'subscription_service_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        $url = $this->generateUrl( 'subscription_service_form_update', ['id' => $id])
               . "#sublist_block";
        return new RedirectResponse($url);
    }

    #[Route('/admin/subscription_services/{id}/service_items/{item_id}', methods:["PUT", "POST"])]
    public function updateSubscriptionServiceServiceItem(int $id, int $item_id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('service_item') && 
                   $input->has('amount') ))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $subscriptionService = $entityManager->getRepository(SubscriptionService::class)->find($id);
            $serviceItem = $entityManager->getRepository(ServiceItem::class)->find($input->get('service_item'));

            if(!$subscriptionService || !$serviceItem)
            { 
                throw $this->createNotFoundException( 'Услуга или абонентский пакет не существуют!' );
            }

            $items = \iterator_to_array($subscriptionService->getServiceItemSubscriptionServices());
            $serviceItemSubscriptionServices = array_filter($items, function ($x) use ($item_id)
            {
                return $x->getServiceItemMap()->getId() ==  $item_id;
            });

            if(!($serviceItemSubscriptionServices && count($serviceItemSubscriptionServices) == 1))
            {

                throw $this->createNotFoundException( 'Такой связи нету' );
            }
            $serviceItemSubscriptionService = array_pop($serviceItemSubscriptionServices);

            $serviceItemSubscriptionService->setServiceItemMap($serviceItem);
            $serviceItemSubscriptionService->setAmount($input->get('amount'));

            $entityManager->flush();



        } catch(BadRequestHttpException $err_text)
        {
            return $this->redirectToRoute(
                'subscription_service_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        $url = $this->generateUrl( 'subscription_service_form_update', ['id' => $id, 'success_sub_id' => $serviceItem->getId()])
               . "#service_item_subscription_service_". $serviceItemSubscriptionService->getId();
        return new RedirectResponse($url);
    }

    #[Route('/admin/subscription_services/{id}/service_items/{item_id}', methods:["DELETE"])]
    public function removeSubscriptionServiceServiceItem(int $id, int $item_id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $subscriptionService = $entityManager->getRepository(SubscriptionService::class)->find($id);

        if(!$subscriptionService)
        { 
            throw $this->createNotFoundException( 'Такой связи нету' );
        }


        $items = \iterator_to_array($subscriptionService->getServiceItemSubscriptionServices());
        $serviceItemSubscriptionServices = array_filter($items, function ($x) use ($item_id)
        {
            return $x->getServiceItemMap()->getId() ==  $item_id;
        });

        if(!($serviceItemSubscriptionServices && count($serviceItemSubscriptionServices) == 1))
        {

            throw $this->createNotFoundException( 'Такой связи нету' );
        }

        $serviceItemSubscriptionService = array_pop($serviceItemSubscriptionServices);
        $id = $serviceItemSubscriptionService->getId();

        $entityManager->getRepository(ServiceItemSubscriptionService::class)->remove($serviceItemSubscriptionService, true);

        return new JsonResponse(["id" => $id ]);
    }


    #[Route('/admin/subscription_services/{id}', methods:["DELETE"])]
    public function remove(int $id, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(SubscriptionService::class);
        $subscriptionService = $repository->find($id);

        if(!$subscriptionService)
        { 
            throw $this->createNotFoundException( 'Нет абонентского обслуживания с id = '.$id.'!' );
        }

        $repository->remove($subscriptionService, true);

        return new JsonResponse(["ok"]);

    }
    

    

}