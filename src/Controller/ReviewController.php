<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Review;
use App\Entity\Service;
use App\Entity\ServiceItem;
use App\Entity\SubscriptionService;
use App\Utils\FileUploader;
use App\Utils\ImgUploader;
use App\Utils\JsonBlockLoader;
use App\Utils\PaginateHelper;
use App\Utils\SEOHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends AbstractController
{
    #[Route('/reviews', name: 'reviews', methods:["GET"])]
    public function index(Request $request, ManagerRegistry $doctrine, JsonBlockLoader $jsonBlockLoader): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request, 8);

        $reviewRepository = $doctrine->getRepository(Review::class);
        $paginator = $reviewRepository->getListPaginator($paginateHelper);

        $json_blocks =  $jsonBlockLoader->load($doctrine);

        return $this->render('user/single_pages/reviews.html.twig', [
            'json_blocks' => $json_blocks,
            'seo' => SEOHelper::getSEOFromJsonBlockData($json_blocks, "reviews", "article") ,
            'service_banner' => $doctrine->getRepository(Service::class)->findRandom(20),
            'subscription_services_banner'  => $doctrine->getRepository(SubscriptionService::class)->findAll(),
            'persons_banner' => $doctrine->getRepository(Person::class)->findTopRank(3),
            'reviews' => $paginator 
        ] + $paginateHelper -> getViewInfo($reviewRepository->getNum(), 10)
       );
    }


    #[Route('/admin/reviews', name: 'admin_reviews', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $reviewRepository = $doctrine->getRepository(Review::class);
        $paginator = $reviewRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/reviews/index.html.twig', [
            'title' => 'Отзывы',
            'files_dir' => 'reviews',
            'reviews' => $paginator 
        ] + $paginateHelper -> getViewInfo($reviewRepository->getNum(), 10)
       );
    }

    #[Route('/admin/reviews/form', methods:["GET"], name:'review_form_new')]
    public function addForm(Request $request, ManagerRegistry $doctrine): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;
        
        $repo = $doctrine->getRepository(ServiceItem::class);

        $serviceItems = $repo->findAll();
        usort($serviceItems, fn ($a, $b) => $a->getId() > $b->getId());


        return $this->render('admin/reviews/form_new.html.twig', [
            'title' => 'Добавить новый отзыв',
            'service_items' => $serviceItems,
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/reviews', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        $input = $request->request;
        $file = $request->files;

        try
        {
            if( !( $input->has('author') && 
                   \mb_strlen($input->get('author')) >= 4 &&
                   $input->has('company_name') && 
                   \mb_strlen($input->get('company_name')) >= 4 &&
                   $input->has('text') && 
                   \mb_strlen($input->get('text')) >= 4) &&
                   $input->has('service'))
            {
                throw new BadRequestHttpException("Автор отзыва, пояснение и текст отзыва должены содержать не менее 4 символов");
            }

            $serviceItem = $doctrine->getRepository(ServiceItem::class)->find($input->get('service'));

            if(!$serviceItem)
            { 
                throw $this->createNotFoundException( 'Услуга не существуют!' );
            }

            $review = new Review();
            $review->setAuthor($input->get('author'));
            $review->setCompanyName($input->get('company_name'));
            $review->setText($input->get('text'));
            $review->setServiceItem($serviceItem);
            $review->setCreatedAt(new \DateTimeImmutable());


            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/reviews', FileUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(500, 700);
                $uploader->saveImg([100, 200, 400]);

                $review->setUrl($uploader->getPath());
               
            };

            $doctrine->getRepository(Review::class)->save($review, true);

            return $this->redirectToRoute('admin_reviews'); 

        } catch( BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'review_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/reviews/{id}', methods:["GET"], name:'review_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Review::class);
        $review = $repository->find($id);
        
        if(!$review)
        { 
            throw $this->createNotFoundException( 'Нет отзыва с id = '.$id.'!' );
        }

        $query_params = $request->query;

        $repo = $doctrine->getRepository(ServiceItem::class);
        $serviceItems = $repo->findAll();
        usort($serviceItems, fn ($a, $b) => $a->getId() > $b->getId());

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/reviews/form_update.html.twig', [
            'title' => 'Отзыв: "'.$review->getAuthor().'"',
            'review' => $review,
            'files_dir' => 'reviews',
            'service_items' => $serviceItems,
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/reviews/{id}', methods:["PUT", "POST"])]
    public function updateReview(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('author') && 
                   \mb_strlen($input->get('author')) >= 4 &&
                   $input->has('company_name') && 
                   \mb_strlen($input->get('company_name')) >= 4 &&
                   $input->has('text') && 
                   \mb_strlen($input->get('text')) >= 4) &&
                   $input->has('service'))
            {
                throw new BadRequestHttpException("Автор отзыва, пояснение и текст отзыва должены содержать не менее 4 символов");
            }

            $review = $entityManager->getRepository(Review::class)->find($id);

            if(!$review)
            { 
                throw $this->createNotFoundException( 'Нет отзыва с id = '.$id.'!' );
            }

            $serviceItem = $doctrine->getRepository(ServiceItem::class)->find($input->get('service'));

            if(!$serviceItem)
            { 
                throw $this->createNotFoundException( 'Услуга не существуют!' );
            }

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/reviews', FileUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(500, 700);
                $uploader->saveImg([100, 200, 400]);

                $review->setUrl($uploader->getPath());
            }
            
            $review->setAuthor($input->get('author'));
            $review->setCompanyName($input->get('company_name'));
            $review->setText($input->get('text'));
            $review->setServiceItem($serviceItem);

            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'review_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'review_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }



   #[Route('/admin/reviews/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(Review::class);
       $review = $repository->find($id);

       if(!$review)
       { 
           throw $this->createNotFoundException( 'Нет отзыва с id = '.$id.'!' );
       }

       $repository->remove($review, true);

       return new JsonResponse(["ok"]);

   }

}