<?php

namespace App\Controller;

use App\Entity\AboutItem;
use App\Utils\ImgUploader;
use App\Utils\PaginateHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AboutItemController extends AbstractController
{
    #[Route('/admin/about_items', name: 'admin_about_items', methods:["GET"])]
    public function adminIndex(Request $request, ManagerRegistry $doctrine): Response
    {
        $paginateHelper = PaginateHelper::createFormRequest($request,8);

        $aboutItemRepository = $doctrine->getRepository(AboutItem::class);
        $paginator = $aboutItemRepository->getListPaginator($paginateHelper);
        
        return $this->render('admin/about_items/index.html.twig', [
            'title' => 'О компании (блоки)',
            'files_dir' => 'about_items',
            'about_items' => $paginator 
        ] + $paginateHelper -> getViewInfo($aboutItemRepository->getNum(), 10)
       );
    }

    #[Route('/admin/about_items/form', methods:["GET"], name:'about_item_form_new')]
    public function addForm(Request $request): Response
    { 
        $query_params = $request->query;

        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;

        return $this->render('admin/about_items/form_new.html.twig', [
            'title' => 'Добавить новое блок "О компании"',
            'error' => $err,
            'error_text' => $error_text
        ]);
    }

    #[Route('/admin/about_items', methods:["POST"])]
    public function add(Request $request,  ManagerRegistry $doctrine): Response
    { 
        $input = $request->request;
        $file = $request->files;

        try
        {
            if( !( $input->has('text') && 
                   $file->has('file') && 
                   !is_null($file->get('file'))))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $aboutItem = new AboutItem();
            $aboutItem->setText($input->get('text'));
            $uploader = new ImgUploader($file->get('file'), 'files/about_items', ImgUploader::$JPEG_PNG_MIME);
            $uploader->loadImg(600, 600, 1.4, 0.6);
            $uploader->saveImg([100, 200, 300, 600]);

            $aboutItem->setImg($uploader->getPath());

            $doctrine->getRepository(AboutItem::class)->save($aboutItem, true);

            return $this->redirectToRoute('admin_about_items'); 

        } catch( BadRequestHttpException | FileException $err)
        {

            return $this->redirectToRoute(
                'about_item_form_new',
                ['err' => 1, 'error_text' => $err->getMessage()]
            );
        }
    }

    #[Route('/admin/about_items/{id}', methods:["GET"], name:'about_item_form_update')]
    public function updateForm(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(AboutItem::class);
        $aboutItem = $repository->find($id);
        
        if(!$aboutItem)
        { 
            throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
        }

        $query_params = $request->query;
        $err = ($query_params->has('err'))? boolval($query_params->get('err')): false;
        $success = ($query_params->has('success'))? boolval($query_params->get('success')): false;
        $error_text = ($query_params->has('error_text'))? $query_params->get('error_text'): false;;

        return $this->render('admin/about_items/form_update.html.twig', [
            'title' => 'Блок о компании: id="'.$aboutItem->getId().'"',
            'about_item' => $aboutItem,
            'files_dir' => 'about_items',
            'error' => $err,
            'success' => $success ,
            'error_text' => $error_text
        ]);
    }


    #[Route('/admin/about_items/{id}', methods:["PUT", "POST"])]
    public function updateAboutItem(int $id,  Request $request, ManagerRegistry $doctrine): Response
    {
        $input = $request->request;
        $file = $request->files;

        $entityManager = $doctrine->getManager();

        try
        {
            if( !( $input->has('text')))
            {
                throw new BadRequestHttpException("Не переданы обязательные данные");
            }

            $aboutItem = $entityManager->getRepository(AboutItem::class)->find($id);

            if(!$aboutItem)
            { 
                throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
            }

            if($file->has('file') && !is_null($file->get('file')))
            {
                $file = $request->files->get('file');
                $uploader = new ImgUploader($file, 'files/about_items', ImgUploader::$JPEG_PNG_MIME);
                $uploader->loadImg(300, 300, 1.2, 0.8);
                $uploader->saveImg([100, 200, 300]);

                $aboutItem->setImg($uploader->getPath());
            }
            
            $aboutItem->setText($input->get('text'));

            $entityManager->flush();



        } catch(BadRequestHttpException | FileException $err_text)
        {
            return $this->redirectToRoute(
                'about_item_form_update',
                ['id' => $id, 
                 'err' => 1, 
                 'error_text' => $err_text->getMessage()]
            );
        }

        return $this->redirectToRoute(
            'about_item_form_update',
            ['id' => $id, 
             'success' => 1, 
             'err' => 0]
        );
    }

   #[Route('/admin/about_items/{id}', methods:["DELETE"])]
   public function remove(int $id, ManagerRegistry $doctrine): Response
   {
       $repository = $doctrine->getRepository(AboutItem::class);
       $aboutItem = $repository->find($id);

       if(!$aboutItem)
       { 
           throw $this->createNotFoundException( 'Нет преимущества с id = '.$id.'!' );
       }

       $repository->remove($aboutItem, true);

       return new JsonResponse(["ok"]);

   }
    

    

}