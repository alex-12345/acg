<?php

namespace App\Entity;

use App\UserInterface\EntityPriceSortableInterface;
use App\Repository\ServiceItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServiceItemRepository::class)]
class ServiceItem implements EntityPriceSortableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 400)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $price = null;

    #[ORM\ManyToOne(inversedBy: 'serviceItems')]
    private ?Service $Service = null;

    #[ORM\OneToMany(mappedBy: 'serviceItemMap', targetEntity: ServiceItemSubscriptionService::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $serviceItemSubscriptionServices;

    public function __construct()
    {
        $this->serviceItemSubscriptionServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->Service;
    }

    public function setService(?Service $Service): self
    {
        $this->Service = $Service;

        return $this;
    }

    /**
     * @return Collection<int, ServiceItemSubscriptionService>
     */
    public function getServiceItemSubscriptionServices(): Collection
    {
        return $this->serviceItemSubscriptionServices;
    }
    
    public function getIntPrice(): int
    {

        $price_str = $this->getPrice();
        if(str_contains(mb_strtolower($price_str), "договор"))
        {
            return PHP_INT_MAX;
        }

        if(preg_replace("/[^0-9]/", "", $price_str) == "")
        {
            return 0;
        }

        $price_str = preg_replace("/[^0-9\,\.]/", "", $price_str);
        return intval(floatval($price_str));
    }

}
