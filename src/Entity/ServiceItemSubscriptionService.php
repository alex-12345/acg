<?php

namespace App\Entity;

use App\Repository\ServiceItemSubscriptionServiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[ORM\Entity(repositoryClass: ServiceItemSubscriptionServiceRepository::class)]
#[UniqueConstraint(name: "service_item_subscription_service_index", fields: ['serviceItemMap', 'subscriptionServiceMap'])]
class ServiceItemSubscriptionService
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'serviceItemSubscriptionServices')]
    private ?ServiceItem $serviceItemMap = null;

    #[ORM\ManyToOne(inversedBy: 'serviceItemSubscriptionServices')]
    private ?SubscriptionService $subscriptionServiceMap = null;

    #[ORM\Column(length: 255)]
    private ?string $amount = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceItemMap(): ?ServiceItem
    {
        return $this->serviceItemMap;
    }

    public function setServiceItemMap(?ServiceItem $serviceItemMap): self
    {
        $this->serviceItemMap = $serviceItemMap;

        return $this;
    }

    public function getSubscriptionServiceMap(): ?SubscriptionService
    {
        return $this->subscriptionServiceMap;
    }

    public function setSubscriptionServiceMap(?SubscriptionService $subscriptionServiceMap): self
    {
        $this->subscriptionServiceMap = $subscriptionServiceMap;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
