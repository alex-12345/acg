<?php

namespace App\Entity;

use App\UserInterface\EntityPriceSortableInterface;
use App\Repository\SubscriptionServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SubscriptionServiceRepository::class)]
class SubscriptionService implements EntityPriceSortableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $price = null;

    #[ORM\Column(length: 255)]
    private ?string $img = null;

    #[ORM\OneToMany(mappedBy: 'subscriptionServiceMap', targetEntity: ServiceItemSubscriptionService::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $serviceItemSubscriptionServices;

    #[ORM\Column(length: 255)]
    private ?string $subTitle = null;

    public function __construct()
    {
        $this->serviceItemSubscriptionServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection<int, ServiceItemSubscriptionService>
     */
    public function getServiceItemSubscriptionServices(): Collection
    {
        return $this->serviceItemSubscriptionServices;
    }

    public function getSubTitle(): ?string
    {
        return $this->subTitle;
    }

    public function setSubTitle(string $subTitle): self
    {
        $this->subTitle = $subTitle;

        return $this;
    }


    public function getIntPrice(): int
    {

        $price_str = $this->getPrice();
        if(str_contains(mb_strtolower($price_str), "договор"))
        {
            return PHP_INT_MAX;
        }

        if(preg_replace("/[^0-9]/", "", $price_str) == "")
        {
            return 0;
        }

        $price_str = preg_replace("/[^0-9\,\.]/", "", $price_str);
        return intval(floatval($price_str));
    }

}
