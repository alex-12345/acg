<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Utils\ServiceType;
use App\Utils\Sorter;

#[ORM\Entity(repositoryClass: ServiceRepository::class)]
class Service
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique:true)]
    private ?string $name = null;

    #[ORM\Column]
    private ?ServiceType $type = null;

    #[ORM\OneToMany(mappedBy: 'Service', targetEntity: ServiceItem::class)]
    private Collection $serviceItems;

    #[ORM\Column(length: 255, unique:true)]
    private ?string $canonicalUrl = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $img = null;

    public function __construct()
    {
        $this->serviceItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?ServiceType
    {
        return $this->type;
    }

    public function setType(ServiceType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ServiceItem>
     */
    public function getServiceItems(): Collection
    {
        $all = \iterator_to_array($this->serviceItems);
        $sortedAll = Sorter::sortByPrice($all);
        return new ArrayCollection($sortedAll);
    }

    public function addServiceItem(ServiceItem $serviceItem): self
    {
        if (!$this->serviceItems->contains($serviceItem)) {
            $this->serviceItems->add($serviceItem);
            $serviceItem->setService($this);
        }

        return $this;
    }

    public function removeServiceItem(ServiceItem $serviceItem): self
    {
        if ($this->serviceItems->removeElement($serviceItem)) {
            // set the owning side to null (unless already changed)
            if ($serviceItem->getService() === $this) {
                $serviceItem->setService(null);
            }
        }

        return $this;
    }

    public function getCanonicalUrl(): ?string
    {
        return $this->canonicalUrl;
    }

    public function setCanonicalUrl(string $canonicalUrl): self
    {
        $this->canonicalUrl = $canonicalUrl;

        return $this;
    }

    public function genCanonicalUrl(): self
    {
        $slugify = new Slugify();

        $this->canonicalUrl = $slugify->slugify($this->getName());

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }
}
