<?php
declare(strict_types=1);

namespace App\Utils;

use App\Entity\JSONBlock;

class BuildInDataHelper
{
    static private $data = [
        JSONBlock::class => [
            [
                "key" => "header_menu_data",
                "description" => "Информация для верхнего блока меню",
                "data" => [
                    "address" => "Москва, Научный проезд дом 19",
                    "tel" => ["+7 (495) 540 54-40", "+74955405440"],
                    "work_time" => "Пн-Пт с 9:00 до 18:00",
                    "email_time" => "Пн-Пт с 9:00 до 18:00",
                    "email" => "info@amazon-city.su",
                    "telegram" => "https://t.me/79197670777",
                    "whats_app" => "https://wtsapp.online/79197670777"
                ]
            ], 
            [
                "key" => "main_page_lable",
                "description" => "Информация под логотипом на главной странице",
                "data" => [
                    "title" => "ПРОФЕССИОНАЛЬНАЯ\n КОМАНДА ЮРИСТОВ",
                    "subtitle" => "С 2012 ГОДА НАШИ СПЕЦИАЛИСТЫ ОКАЗЫВАЮТ ЮРИДИЧЕСКУЮ ПОДДЕРЖКУ ФИЗИЧЕСКИМ И ЮРИДИЧЕСКИМ ЛИЦАМ. ".
                                  "AMAZON CITY GROUP - ЭТО ОБЪЕДИНЕНИЕ ПРОФЕССИОНАЛОВ, ГОТОВЫХ РЕШИТЬ ВАШИ ЮРИДИЧЕСКИЕ ПРОБЛЕМЫ!"
                ]
            ], 
            [
                "key" => "main_page_why_us",
                "description" => "Информация из блока \"Почему мы\" на главной странице",
                "data" => [
                    "main_arg" => [
                        "title" => "ООО \"Амазон Сити Групп\"", 
                        "text" => " - это объединение юристов и адвокатов, каждый из которых имеет юридический стаж около 10 лет. " . 
                                  "Приоритетом компании стало качественное оказание юридических услуг как для физических, так и для юридических лиц. " . 
                                  "Благодаря подобранной команде профессионалов в каждой из областей права, каждое дело, за которое берутся наши специалисты, " .
                                  "оканчивается наиболее благоприятным исходном, а большинство из них чистым выигрышем"
                    ],
                    "sub_args" => [
                        [
                            "title" => "ДОРОЖИМ СВОЕЙ БЕЗУПРЕЧНОЙ РЕПУТАЦИЕЙ", 
                            "text"  => "Мы не беремся за все дела подряд ради комиссии и трезво оцениваем шансы на успех, о чем сообщаем заранее."
                        ],
                        [
                            "title" => "УДОБНАЯ ПОЭТАПНАЯ ОПЛАТА НАШИХ УСЛУГ", 
                            "text"  => "Так как многие дела связаны с финансовой стороной и сопровождаются тратами мы предлагаем поэтапную оплату"
                        ],
                        [
                            "title" => "ШТАТ СПЕЦИАЛИСТОВ-ПРОФЕССИОНАЛОВ", 
                            "text"  => "Для вас работает целая команда спецов, в которой каждый является экспертом своего направления и области права"
                        ],
                    ]
                ]
            ], 
            [
                "key" => "main_page_subscription_comment",
                "description" => "Комментарий к блоку \"Абонентское обслуживание\" на главной странице",
                "data" => ["* Также предлагаем нашим Клиентам согласовать индивидуальный пакет юридических услуг для бизнеса. ".
                          "Мы включим сформируем наиболее оптимальный пакет юридических услуг,  которые требуются вашему бизнесу."]
            ],  
            [
                "key" => "page_subscription_info",
                "description" => "Информация со страницы \"Юридическое сопровождение бизнеса\"",
                "data" => [
                    "subtitle" => "Чем мы можем помочь?",
                    "description" => "Если в вашей компании нет штатных юристов, отсутствует отдел кадров, или вы желаете оптимизировать свои расходы по части зарплат,".
                                    " то юридический аутсорсинг от компании Amazon City Group будет для вас наилучшим выбором. При расходах, как на одного сотрудника, ".
                                    "над юридической защитой вашего бизнеса будет работать целая команда профессионалов, имеющих большой стаж в каждой из областей права."
                ]
            ],   
            [
                "key" => "work_order",
                "description" => "Блок \"Порядок работ\"",
                "data" => [
                    "title" => "Порядок работ",
                    "data" => [
                        1 => [ 
                            "name" => "Начало работы", 
                            "description" => "Клиент вправе направить Заявку с заданием для ООО «АМАЗОН СИТИ ГРУПП» в любой удобной ему форме и приемлемое время ".
                                             "– электронная почта, телефон, личная встреча"
                        ],
                        2 => [ 
                            "name" => "Предворительная консультация", 
                            "description" => "Клиент вправе направить Заявку с заданием для ООО «АМАЗОН СИТИ ГРУПП» в любой удобной ему форме и приемлемое время ".
                                             "– электронная почта, телефон, личная встреча"
                        ],
                        3 => [ 
                            "name" => "Заключение договора", 
                            "description" => "Клиент вправе направить Заявку с заданием для ООО «АМАЗОН СИТИ ГРУПП» в любой удобной ему форме и приемлемое время ".
                                             "– электронная почта, телефон, личная встреча"
                        ],
                        4 => [ 
                            "name" => "Оказание услуг по договору", 
                            "description" => "Клиент вправе направить Заявку с заданием для ООО «АМАЗОН СИТИ ГРУПП» в любой удобной ему форме и приемлемое время ".
                                             "– электронная почта, телефон, личная встреча"
                        ]
                    ]
                ]
            ], 
            [
                "key" => "footer_data",
                "description" => "Информация для самого нижнего блока сайта",
                "data" => [
                    "about" => [
                        "title" => "О нас", 
                        "text" => "Юридическая компания «Амазон Сити Групп» является объединением  юристов, придерживающихся высоких ".
                                  "стандартов при решении юридических проблем физических и юридических лиц.<br><br>Информация на сайте носит справочный характер и не является публичной офертой, определяемой положениями ст. 437 ГК РФ"
                    ],
                    "contacts" => [
                        "title" => "Контакты", 
                        "tels" => [ 
                            [ "+7 (495) 540 54-40", "+74955405440"],
                            [ "+7 (919) 767 07-77", "+79197670777"]
                        ],
                        "email" => "info@amazon-city.su",
                        "address" => "Москва, Научный проезд дом 19",
                        "work_time" => "Пн-Пт с 9:00 до 18:00",
                        "telegram" => "https://t.me/79197670777",
                        "whats_app" => "https://wtsapp.online/79197670777"
                    ]
                ]
            ], 
            [
                "key" => "contact_page",
                "description" => "Вся информация со страницы \"Контакты\"",
                "data" => [
                    "route" => [
                        "title" => "Популярный пеший маршрут от ст. метро «Калужская»",
                        "time" => "~15мин.",
                        "metro" => [
                            "line" => [
                                "title" => "Калужско-Рижская линия Московского метрополитена",
                                "number" => "6",
                                "color" => [
                                    "title" => "Оранжевая ветка",
                                    "hex" => "#ffa500"
                                ] 
                            ]
                        ],
                        "steps" => [ "Профсоюзная 59 к1", "Научный проезд 8", "Научный проезд 18", "БЦ \"9 Акров\"" ] 
                    ], 
                    "info_block" => [
                        "title" => "Контакные данные",
                        "company_name" => "ООО «АМАЗОН СИТИ ГРУПП»",
                        "address" => [
                            "title" => "Фактический адрес",
                            "data" => "Москва, Научный проезд дом 19"
                        ],
                        "tels" => [ 
                            [ "+7 (495) 540 54-40", "+74955405440"],
                            [ "+7 (919) 767 07-77", "+79197670777"]
                        ],
                        "telegram" => "https://t.me/79197670777",
                        "whats_app" => "https://wtsapp.online/79197670777",
                        "email" => "info@amazon-city.su",
                        "work_time" => "Пн-Пт с 9:00 до 18:00",
                        "other_info_blocks" => [
                            [ 
                                "title" => "Юридический адрес", 
                                "data" => "108850, г.Москва, вн. тер. г. Поселение Внуковское, ул. Лётчика Ульянина, д. 3а, помещ. 155"
                            ],
                            [ 
                                "title" => "ОГРН", 
                                "data" => "1227700069926"
                            ],
                            [ 
                                "title" => "ИНН/КПП", 
                                "data" => "7751215242/775101001"
                            ],
                            [ 
                                "title" => "Генеральный директор", 
                                "data" => "Горбачева Софья Сергеевна"
                            ]
                        ]
                    ],
                    "map_link" => "https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0c296cd7c5460134f3fb526ad9f321e2cbe373b95ed622d885288cfef9e23edb&amp;width=100%25&amp;height=397&amp;lang=ru_RU&amp;scroll=true"
                ]
            ],
            [
                "key" => "redirected",
                "description" => "Редиректы со старых URL",
                "data" => [
                    "/o-kompanii/" => "/about",
                    "/o-kompanii" => "/about",
                    "/novosti/" => "/blogs",
                    "/novosti" => "/blogs"
                ]

            ],
            [
                "key" => "seo",
                "description" => "Статические seo заголовки",
                "data" => [
                    "default" => 
                        [ 
                            "title_postfix" => "Amazon City Group",
                            "site_name" => "Amazon City Group - Юридическая компания",
                            "title" => "Профессиональная команда юристов",
                            "description" => "Узнать стоимость платной юридической консультации в Москве. ⚖️ Где получить услугу консультации юриста онлайн и по телефону. Консультация юриста Amazon-City +7 (495) 540 54-40."
                        ],
                    "contacts" => [
                        "title" => "Контакты",
                        "description" => "Контакты юридической фирмы в Москве Amazon City Group. Все виды юридических услуг от работы с документами до представительства в судах."
                    ],
                    "blogs" => [
                        "title" => "Новости компании",
                        "description" => "Раздел новостей содержит всю самую последнюю информацию о деятельности и успехах компании."
                    ],
                    "prices" => [
                        "title" => "Цены на юридические услуги",
                        "description" => "Полный список цен на все предоставляемые юридические услугии Amazon City Group. Все виды юридических услуг от работы с документами до представительства в судах."
                    ],
                    "docs" => [
                        "title" => "Документы",
                        "description" => "Документы юридической фирмы Amazon City Group. Все виды юридических услуг от работы с документами до представительства в судах."
                    ],
                    "about" => [
                        "title" => "О юридической компании",
                        "description" => "Юридическая компания Amazon City Group предлагает услуги по юридическому сопровождению бизнеса, а также защите интересов физических и юр лиц."
                    ],
                    "services/legal_entities" => 
                    [
                        "title" => "Услуги организациям",
                        "description" => "Команда юристов оказывает услуги по участию в переговорах, аудите контрагентов, регистрации юр. лиц и разработке учредительных документов."
                    ],
                    "services/individuals" => 
                    [
                        "title" => "Услуги гражданам",
                        "description" => "Юристы и адвокаты по гражданским делам: защита прав потребителя, трудовые и страховые споры, возмещение ущерба при ДТП, составление договоров."
                    ],
                    "subscription_services" => 
                    [
                        "title" => "Юридическое сопровождение бизнеса",
                        "description" => "Полное юридическое сопровождение для бизнеса от профессиональной команды юристов. Работаем как с небольшими, так и крупными компаниями."
                    ],
                    "reviews" => 
                    [
                        "title" => "Отзывы о компании",
                        "description" => "Отзывы о компании AMAZON CITY GROUP. Отзывы по юридическим услугам в Москве: защита прав потребителя, трудовые и страховые споры, возмещение ущерба при ДТП, составление договоров"
                    ],
                    "persons" => 
                    [
                        "title" => "Сотрудники",
                        "description" => "В AMAZON CITY GROUP работает целая команда спецов, в которой каждый является экспертом своего направления и области права"
                    ],
                    "" => 
                    [
                        "title" => "Профессиональная команда юристов",
                        "description" => "Узнать стоимость платной юридической консультации в Москве. ⚖️ Где получить услугу консультации юриста онлайн и по телефону. Консультация юриста Amazon-City +7 (495) 540 54-40."
                    ]
                ]


                    ],
            [
                "key" => "titles",
                "description" => "Отображаемые статические заголовки и подзаголовки на страницах",
                "data" => [
                    "contacts" => 
                    [
                        "title" => "Схема проезда",
                        "sub_title" => "Как нас найти?"
                    ],
                    "blogs" => 
                    [
                        "title" => "Новости",
                        "sub_title" => "Наш пресс центр"
                    ],
                    "prices" => 
                    [
                        "title" => "Цены",
                        "sub_title" => "Наши"
                    ],
                    "docs" => 
                    [
                        "title" => "Документы",
                        "sub_title" => "Наши"
                    ],
                    "about" => 
                    [
                        "title" => "О юридической компании",
                        "sub_title" => "Кто мы?"
                    ],
                    "services/legal_entities" => 
                    [
                        "title" => "Услуги организациям",
                        "sub_title" => "Услуги"
                    ],
                    "services/individuals" => 
                    [
                        "title" => "Услуги гражданам",
                        "sub_title" => "Услуги"
                    ],
                    "subscription_services" => 
                    [
                        "title" => "Юридическое сопровождение бизнеса",
                        "sub_title" => "Чем мы можем помочь?"
                    ],
                    "reviews" => 
                    [
                        "title" => "Отзывы Клиентов",
                        "sub_title" => "Что говорят о нас?"
                    ],
                    "persons" => 
                    [
                        "title" => "Познакомьтесь с нашими экспертами",
                        "sub_title" => "Квалифицированные юристы?"
                    ]
                ]
            ]
        ]
    ];
    static public function getBuildInData(string $entityName)
    {
        return self::$data[$entityName];

    }
}
