<?php
declare(strict_types=1);

namespace App\Utils;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\MimeTypes;

class FileUploader
{
    public static array $ZIP_PDF_MIME = ['application/pdf', 'application/zip']; 
    public static array $JPEG_PNG_MIME = ['image/jpeg', 'image/png']; 
    
    public UploadedFile $file;
    public string $dir;
    public array $mime;
    public string $path;

    public function __construct(UploadedFile $file, string $dir, array $mime)
    {
        $this->file = $file;
        $this->dir = $dir;
        $this->mime = $mime;
        $this->path = md5($this->file->getClientOriginalName()). '-'. strval(time()). "." . 
                (new MimeTypes())->getExtensions($this->file->getMimeType())[0];
    }

    public function checkMimeAndSize(int $maxSizeMb = 15): void
    {
        if(!in_array($this->file->getMimeType(), $this->mime))
        {
            throw new FileException("Недопустимый формат файла. Допустимые форматы: ". implode(',', $this->mime));
        }

        if($this->file->getSize() > $maxSizeMb * 1024*1024)
        {
            throw new FileException("Слишком большой файл. Не более ". $maxSizeMb ." Мб!");
        }

    }

    public function upload(): string
    {   
        $path = md5($this->file->getClientOriginalName()). '-'. strval(time()). "." . 
                (new MimeTypes())->getExtensions($this->file->getMimeType())[0];

        $this->path = $path;

        if (!file_exists($this->dir)) {
            mkdir($this->dir , 0777, true);
        }

        if (!file_exists($this->dir . '/original')) {
            mkdir($this->dir . '/original' , 0777, true);
        }

        $this->file->move($this->dir . '/original', $path);

        return $path;

    }


    public function getPath(): string
    {
        return $this->path;

    }



}