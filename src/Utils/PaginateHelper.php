<?php
declare(strict_types=1);

namespace App\Utils;


use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

class PaginateHelper
{
    private int $number;
    private int $size;
    private bool $isNeedStart;
    private bool $isNeedLast;

    private function __construct(int $number, int $size)
    {
        $this->number = $number;
        $this->size = $size;
        $this->isNeedStart = false;
        $this->isNeedLast = false;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function isNeedStart(): bool
    {
        return $this->isNeedStart;
    }

    public function isNeedLast(): bool
    {
        return $this->isNeedLast;
    }

    public function getFirstResult(): int
    {
        return ($this->number - 1) * $this->size;
    }

    public static function createFormRequest(Request $request, int $default_size = 10): self
    {
        try {
            $param = $request->query->all('page');
            $number = (isset($param['number']) && intval($param['number']) > 0) ? intval($param['number']) : 1;
            $size = (isset($param['size']) && intval($param['size']) > 0) ? intval($param['size']) : $default_size;
        } catch (BadRequestException $exception) {
            $number = 1;
            $size = $default_size;
        }

        return new self($number, $size);
    }

    private static function getPageNavItem(int $link, string $name, bool $isActive):array
    {
        return ['link' => $link, 'name' => $name, 'isActive' => $isActive];
    }

    private function getPageNav(int $total_num, int $dispayPages = 10):array
    {
        $pageArr = [];
        $totalPages = 1;
        $currentPage = $this->number;

        if($total_num > $this->size)
        {
            $totalPages = intval(ceil($total_num / $this->size));

            if($totalPages <= $dispayPages)
            {
                // умещается
                for($i=0; $i<$totalPages;$i++)
                {
                    $pageArr[] = self::getPageNavItem($i+1, strval($i+1), $i+1 == $currentPage);
                }
            } else
            {
                if($currentPage * 2 <= $dispayPages )
                {
                    // слева
                    for($i=0; $i<$dispayPages-1;$i++)
                    {
                        $pageArr[] = self::getPageNavItem($i+1, strval($i+1), $i+1 == $currentPage);
                    }
                    $pageArr[] = self::getPageNavItem($dispayPages, "...", false);
                    $this->isNeedLast = true;
                    
                } else if(($totalPages - $currentPage) * 2 <= $dispayPages)
                {
                    // спрва
                    $pageArr[] = self::getPageNavItem($totalPages - $dispayPages + 1, "...", false);
                    $this->isNeedStart = true;

                    for($i=$totalPages - $dispayPages + 1; $i<$totalPages;$i++)
                    {
                        $pageArr[] = self::getPageNavItem($i+1, strval($i+1), $i+1 == $currentPage);
                    }
                } else
                {
                    // и слева и справа
                    $start = $currentPage - intval($dispayPages /2) + 1;
                    $end = $currentPage + intval($dispayPages /2) + 1;

                    $pageArr[] = self::getPageNavItem($start, "...", false);
                    $this->isNeedStart = true;

                    for($i=$start; $i<$end-2;$i++)
                    {
                        $pageArr[] = self::getPageNavItem($i+1, strval($i+1), $i+1 == $currentPage);
                    }

                    $pageArr[] = self::getPageNavItem($end-1, "...", false);
                    $this->isNeedLast = true;

                }

            }

        }

        return [ 'total_pages' =>  $totalPages, 'page_arr' =>  $pageArr ];
    }

    public function getViewInfo(int $total_num, int $dispayPages = 10)
    {
        $pageNav = $this->getPageNav($total_num, $dispayPages);
        
        return [
            'total_pages' => $pageNav['total_pages'],
            'page_nav' => $pageNav['page_arr'],
            'page_size' => $this->getSize(),
            'is_need_start' => $this->isNeedStart(),
            'is_need_last' => $this->isNeedLast()
        ];

    }
}