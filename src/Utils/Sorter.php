<?php
declare(strict_types=1);

namespace App\Utils;

use App\UserInterface\EntityPriceSortableInterface;
use App\UserInterface\EntitySortableInterface;
use ErrorException;

class Sorter
{

    private static function cmpPrice(EntityPriceSortableInterface $left, EntityPriceSortableInterface $right):int
    {
        $a = $left->getIntPrice();
        $b = $right->getIntPrice();

        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }
    

    public static function sortByPrice(array $arr):array
    {

        usort($arr, [self::class, "cmpPrice"]);
        return $arr;

    }


}