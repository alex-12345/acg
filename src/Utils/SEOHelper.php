<?php
declare(strict_types=1);

namespace App\Utils;

class SEOHelper
{

    static public function getSEOFromData(
        string $title = null, 
        string $description = null,  
        string $canonical = null, 
        string $type = null, 
        string $img = null
    ) : array
    {
        $data = [];

        if ($title)
        {
            $data["title"] = $title;
        }

        if ($description)
        {
            $data["description"] = $description;
        }

        if ($canonical)
        {
            $data["canonical"] = $canonical;
        }
        if ($type)
        {
            $data["type"] = $type;
        }

        if ($img)
        {
            $data["img"] = $img;
        }
        
        return $data;
    }
    static public function getSEOFromJsonBlockData(array $json_blocks, string $canonical_url, $type = null)
    {
        $data = $json_blocks["seo"][$canonical_url];
        $seo = [
            "title" => $data["title"],
            "description" => $data["description"],
            "canonical" => $canonical_url
        ];

        if (array_key_exists("type", $data))
        {
            $seo["type"] = $data["type"];
        } elseif ($type)
        {
            $seo["type"] = $type;
        }


        return $seo;

    }

}