<?php
declare(strict_types=1);

namespace App\Utils;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\ImageManagerStatic;
use Intervention\Image\Image;
use Intervention\Image\Exception\NotReadableException;

class ImgUploader extends FileUploader
{
    public Image $img;

    public function __construct(UploadedFile $file, string $dir, array $mime)
    {
        parent::__construct($file, $dir, $mime);
    }

    /**
     * @throws \Intervention\Image\Exception\NotReadableException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function loadImg(int $min_width, int $min_height, float $max_ratio = 3, float $min_ratio = 0.33)
    {
        $this->checkMimeAndSize(12);

        try
        {
            $img = ImageManagerStatic::make($this->file->getContent());
        } catch ( NotReadableException $err)
        {
            throw new FileException("Ошибка загрузки файла");
        }

        if ( $img->width() < $min_width || 
             $img->height() < $min_height || 
             $img->width() > $min_width * 20 || 
             $img->height() > $min_height * 20 || 
             $img->width() / $img->height() > $max_ratio ||
             $img->width() / $img->height() < $min_ratio )
        {
            throw new FileException(
                "Минимальный размер файла " . $min_width . "x". $min_height . " px, " .
                "максимальный - ". \strval($min_width *10) ."x". strval($min_height *10) ." px, ". 
                "максимальное отношение ширины файла к высоте - ". $max_ratio .", минимальное  - " . $min_ratio . "!");
        }
        
        $this->img = $img;
    }

    public function saveImg(array $additional_width_scales = [], ?array $crop_data_info = null)
    {
        if (!file_exists($this->dir)) {
            mkdir($this->dir , 0777, true);
        }
        
        if (!file_exists($this->dir . '/original')) {
            mkdir($this->dir . '/original' , 0777, true);
        }

        $this->img->save($this->dir . '/original/' . $this->path);
        unset($this->img);

        foreach ($additional_width_scales as $width) {

            $img_tmp = ImageManagerStatic::make($this->dir . '/original/' . $this->path);

            if (!file_exists($this->dir . '/'.$width.'_w')) {
                mkdir($this->dir . '/'.$width.'_w' , 0777, true);
            }

            $img_tmp->resize( $width, null,  function ($constraint) {
                $constraint->aspectRatio();
            });

            $img_tmp->save($this->dir . '/'.$width.'_w/' . $this->path, 100);
            unset($img_tmp);
        }

        if($crop_data_info)
        {
            if (!file_exists($this->dir . '/crop_original')) {
                mkdir($this->dir . '/crop_original' , 0777, true);
            }

            $img_tmp = ImageManagerStatic::make($this->dir . '/original/' . $this->path);
            $sizes = $crop_data_info['sizes'];
            $img_tmp->crop($crop_data_info['width'], $crop_data_info['height'], $crop_data_info['x'], $crop_data_info['y']);
            $img_tmp->save($this->dir . '/crop_original/' . $this->path);
            unset($img_tmp);

            foreach ($sizes as $width) {

                $img_tmp = ImageManagerStatic::make($this->dir . '/crop_original/' . $this->path);
    
                if (!file_exists($this->dir . '/crop_'.$width.'_w')) {
                    mkdir($this->dir . '/crop_'.$width.'_w' , 0777, true);
                }
    
                $img_tmp->resize( $width, null,  function ($constraint) {
                    $constraint->aspectRatio();
                });
    
                $img_tmp->save($this->dir . '/crop_'.$width.'_w/' . $this->path, 100);
                unset($img_tmp);
            }

        }

    }

}