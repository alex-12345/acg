<?php
declare(strict_types=1);

namespace App\Utils;

enum ServiceType: string
{
    case INDIVIDUALS = 'individuals';
    case LEGAL_ENTITIES = 'legal_entities';

    public function text(): string
    {
        return match($this) 
        {
            ServiceType::INDIVIDUALS => 'Для физических лиц',   

            ServiceType::LEGAL_ENTITIES => 'Для юридических лиц'  
        };
    }


}