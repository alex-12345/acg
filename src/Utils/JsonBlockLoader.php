<?php
declare(strict_types=1);

namespace App\Utils;

use App\Entity\JSONBlock;
use DateInterval;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\CacheInterface;

class JsonBlockLoader
{
    private ?CacheInterface $cache;
    private static string $CACHE_NAME = "json_blocks";

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }
    public function load(ManagerRegistry $doctrine)
    {
        $cacheBlocks = $this->cache->getItem(self::$CACHE_NAME);

        if ($cacheBlocks->isHit())
        {
            $this->cache->save($cacheBlocks->expiresAfter(60*60));
            
            return unserialize($cacheBlocks->get());
        } 

        $JSONBlockRepository = $doctrine->getRepository(JSONBlock::class);

        $json_blocks = [];

        foreach( $JSONBlockRepository->findAll() as $item)
        {
            $json_blocks[$item->getKey()] = json_decode($item->getData(), true);
        }

        $cacheBlocks->set(serialize($json_blocks))->expiresAfter(60*60);
        $this->cache->save($cacheBlocks);

        return $json_blocks;
    }

}