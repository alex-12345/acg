<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Events\NewRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TelegramNotificationSubscriber implements EventSubscriberInterface
{
    protected string $chatId;
    protected string $telegarmToken;
    protected HttpClientInterface $httpClient;

    public function __construct(string $chatId, string $telegarmToken, HttpClientInterface $httpClient)
    {
        $this->chatId = $chatId;
        $this->telegarmToken = $telegarmToken;
        $this->httpClient = $httpClient;
    }



    public function onNewRequest(NewRequestEvent $event)
    {
        $clientRequest = $event->getClientRequest();

        $url =  'https://api.telegram.org/bot' .$this->telegarmToken ."/sendMessage";

        $text = "Новая заявка на сайте\n".
                "Имя: ".$clientRequest->getClientName()."\n" .
                "Телефон: " . $clientRequest->getPhone()."\n" .
                "Время: " . $clientRequest->getCreatedAt()->format('d M H:i') . "\n".
                $clientRequest->getText();


        $response = $this->httpClient->request(
            'POST',
            $url,
            [
                'query' => [
                    'chat_id' => $this->chatId,
                    'text' => $text,
                ]
            ]
        );
        // dump($response->toArray());
    }

    public static function getSubscribedEvents()
    {
        return [
            NewRequestEvent::class => 'onNewRequest'
        ];
    }

}