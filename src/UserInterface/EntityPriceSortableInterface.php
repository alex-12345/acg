<?php

namespace App\UserInterface;

interface EntityPriceSortableInterface extends EntitySortableInterface
{

    public function getIntPrice():int;
}