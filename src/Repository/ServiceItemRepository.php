<?php

namespace App\Repository;

use App\Entity\ServiceItem;
use App\Utils\PaginateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServiceItem>
 *
 * @method ServiceItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceItem[]    findAll()
 * @method ServiceItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceItem::class);
    }

    public function save(ServiceItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ServiceItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT s FROM  App\Entity\ServiceItem s ORDER BY s.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    public function findByExludeIds(array $ids)
    {
        $dql = "SELECT s FROM  App\Entity\ServiceItem s WHERE s.id NOT IN (:ids)";
        $query = $this->_em->createQuery($dql)
                ->setParameter('ids', $ids, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        return $query->getResult();

    }

}
