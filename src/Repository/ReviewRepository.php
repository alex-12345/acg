<?php

namespace App\Repository;

use App\Entity\Review;
use App\Utils\PaginateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Review>
 *
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    public function save(Review $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Review $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT r FROM  App\Entity\Review r ORDER BY r.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    public function findRandom(int $num)
    {
        $max_val = 100;
        $dql = "SELECT r FROM  App\Entity\Review r ORDER BY r.id DESC ";
        $query = $this->_em->createQuery($dql)->setMaxResults($max_val);
        $arr = $query->getResult();

        $res = [];
        if(count($arr) < 3)
        {
            return $arr;
        }

        foreach ( array_rand( $arr,  min($max_val,$num,count($arr))) as $key => $value)
        {
            $res[] = $arr[$value];
        }
        return $res;

    }

}
