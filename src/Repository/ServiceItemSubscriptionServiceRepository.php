<?php

namespace App\Repository;

use App\Entity\ServiceItemSubscriptionService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServiceItemSubscriptionService>
 *
 * @method ServiceItemSubscriptionService|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceItemSubscriptionService|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceItemSubscriptionService[]    findAll()
 * @method ServiceItemSubscriptionService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceItemSubscriptionServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceItemSubscriptionService::class);
    }

    public function save(ServiceItemSubscriptionService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ServiceItemSubscriptionService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
}
