<?php

namespace App\Repository;

use App\Entity\Service;
use App\Utils\PaginateHelper;
use App\Utils\ServiceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Service>
 *
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    public function save(Service $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Service $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT s FROM  App\Entity\Service s ORDER BY s.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

   public function findOneByCanonicalUrl($canonical_url): ?Service
   {
       return $this->createQueryBuilder('s')
           ->andWhere('s.canonicalUrl = :canonical_url')
           ->setParameter('canonical_url', $canonical_url)
           ->getQuery()
           ->getOneOrNullResult()
       ;
   }

   public function findAllExcludeOne(Service $service): array
   {
       $entityManager = $this->getEntityManager();

       $query = $entityManager->createQuery(
           'SELECT s
           FROM App\Entity\Service s
           WHERE s.id != :id'
       )->setParameter('id', $service->getId());

       return $query->getResult();
   }

   public function findAllByType(ServiceType $type): array
   {
       $entityManager = $this->getEntityManager();

       $query = $entityManager->createQuery(
           'SELECT s
           FROM App\Entity\Service s
           WHERE s.type = :type 
           ORDER BY s.id DESC'
       )->setParameter('type', $type);

       return $query->getResult();
   }


   public function findRandom(int $num)
    {
        $max_val = 50;
        $dql = "SELECT s FROM  App\Entity\Service s ORDER BY s.id DESC ";
        $query = $this->_em->createQuery($dql)->setMaxResults($max_val);
        $arr = $query->getResult();

        $res = [];
        if(count($arr) < 3)
        {
            return $arr;
        }

        foreach ( array_rand( $arr,  min($max_val,$num, count($arr))) as $key => $value)
        {
            $res[] = $arr[$value];
        }
        return $res;

    }

}
