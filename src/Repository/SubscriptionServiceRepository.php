<?php

namespace App\Repository;

use App\Entity\SubscriptionService;
use App\Utils\PaginateHelper;
use App\Utils\Sorter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SubscriptionService>
 *
 * @method SubscriptionService|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubscriptionService|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubscriptionService[]    findAll()
 * @method SubscriptionService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubscriptionService::class);
    }

    public function findAll():array
    {
        // $cacheAdvataches = $this->cache->getItem(self::$CACHE_NAME);

        // if ($cacheAdvataches->isHit())
        // {
        //     $this->cache->save($cacheAdvataches->expiresAfter(60*60));
            
        //     return unserialize($cacheAdvataches->get());
        // }

        $all = parent::findAll();

        // $cacheAdvataches->set(serialize($all))->expiresAfter(60*60);
        // $this->cache->save($cacheAdvataches);

        $sortedAll = Sorter::sortByPrice($all);

        return $sortedAll;

    }


    public function save(SubscriptionService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SubscriptionService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT s FROM  App\Entity\SubscriptionService s ORDER BY s.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

}
