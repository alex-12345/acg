<?php

namespace App\Repository;

use App\Entity\JSONBlock;
use App\Utils\BuildInDataHelper;
use App\Utils\PaginateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * @extends ServiceEntityRepository<JSONBlock>
 *
 * @method JSONBlock|null find($id, $lockMode = null, $lockVersion = null)
 * @method JSONBlock|null findOneBy(array $criteria, array $orderBy = null)
 * @method JSONBlock[]    findAll()
 * @method JSONBlock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JSONBlockRepository extends ServiceEntityRepository
{
    private ?CacheInterface $cache;
    public static string $CACHE_NAME = "json_blocks";

    public function __construct(ManagerRegistry $registry, CacheInterface $cache)
    {
        parent::__construct($registry, JSONBlock::class);
        $this->cache = $cache;
    }

    public function save(JSONBlock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        $this->cache->delete(self::$CACHE_NAME);
    }

    public function remove(JSONBlock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
        $this->cache->delete(self::$CACHE_NAME);
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT j FROM  App\Entity\JSONBlock j ORDER BY j.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    public function repair()
    {
        $data = BuildInDataHelper::getBuildInData(JSONBlock::class);
        $manager = $this->getEntityManager();
        $manager->getConnection()->exec("DELETE FROM  jsonblock");
        $manager->getConnection()->exec("ALTER SEQUENCE jsonblock_id_seq RESTART WITH 1");

        foreach ($data as $item) {
            $JSONBlock = new JSONBlock();
            $JSONBlock->setKey($item['key']);
            $JSONBlock->setDescription($item['description']);
            $JSONBlock->setData(json_encode($item['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            $manager->persist($JSONBlock);
        }
        $manager->flush();
        $this->cache->delete(self::$CACHE_NAME);
    }

}
