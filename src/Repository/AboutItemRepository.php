<?php

namespace App\Repository;

use App\Entity\AboutItem;
use App\Utils\PaginateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AboutItem>
 *
 * @method AboutItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method AboutItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method AboutItem[]    findAll()
 * @method AboutItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AboutItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AboutItem::class);
    }

    public function save(AboutItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AboutItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT a FROM  App\Entity\AboutItem a ORDER BY a.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }
}
