<?php

namespace App\Repository;

use App\Entity\Advantage;
use App\Utils\PaginateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * @extends ServiceEntityRepository<Advantage>
 *
 * @method Advantage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advantage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advantage[]    findAll()
 * @method Advantage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvantageRepository extends ServiceEntityRepository
{  
    private ?CacheInterface $cache; 
    public static string $CACHE_NAME = "advataches";
    
    public function __construct(ManagerRegistry $registry, CacheInterface $cache)
    {
        parent::__construct($registry, Advantage::class);
        $this->cache = $cache;
    }

    public function findAll():array
    {
        $cacheAdvataches = $this->cache->getItem(self::$CACHE_NAME);

        if ($cacheAdvataches->isHit())
        {
            $this->cache->save($cacheAdvataches->expiresAfter(60*60));
            
            return unserialize($cacheAdvataches->get());
        }

        $all = parent::findAll();

        $cacheAdvataches->set(serialize($all))->expiresAfter(60*60);
        $this->cache->save($cacheAdvataches);

        return $all;

    }

    public function save(Advantage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
        $this->cache->delete(self::$CACHE_NAME);
    }

    public function remove(Advantage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
        $this->cache->delete(self::$CACHE_NAME);
    }

    public function getListPaginator(PaginateHelper $paginateHelper): Paginator
    {
        $dql = "SELECT a FROM  App\Entity\Advantage a ORDER BY a.id DESC";
        $query = $this->_em->createQuery($dql)
            ->setFirstResult($paginateHelper->getFirstResult())
            ->setMaxResults($paginateHelper->getSize());

        return new Paginator($query, false);

    }

    public function getNum()
    {
        return $this->createQueryBuilder('u')
                ->select('count(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }
}
