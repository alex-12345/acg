<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class PersonFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE person_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();

        for ($i = 0; $i < 20; $i++) {
            $person = new Person();
            $person->setFullName($lipsum->words(3));
            $person->setImg('url_ '.$i);
            $person->setRank(rand(1,10));
            $person->setPosition($lipsum->words(4));
            $person->setBriefInfo($lipsum->words(15));
            $person->setQualification($lipsum->words(30));
            $manager->persist($person);

        }
        $manager->flush();

        
    }
}
