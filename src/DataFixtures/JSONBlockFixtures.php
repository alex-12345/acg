<?php

namespace App\DataFixtures;

use App\Entity\JSONBlock;
use App\Utils\BuildInDataHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class JSONBlockFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $arrData = BuildInDataHelper::getBuildInData(JSONBlock::class); 
        
        $manager->getConnection()->exec("ALTER SEQUENCE jsonblock_id_seq RESTART WITH 1");

        foreach ($arrData as $item) {
            $JSONBlock = new JSONBlock();
            $JSONBlock->setKey($item['key']);
            $JSONBlock->setDescription($item['description']);
            $JSONBlock->setData(json_encode($item['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            $manager->persist($JSONBlock);

        }
        $manager->flush();

        
    }

    public static function getGroups(): array
    {
         return ['release'];
    }
}
