<?php

namespace App\DataFixtures;

use App\Entity\Service;
use App\Entity\ServiceItem;
use App\Utils\ServiceType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class ServiceFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE service_id_seq RESTART WITH 1");
        $manager->getConnection()->exec("ALTER SEQUENCE service_item_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();

        for ($i = 0; $i < 35; $i++) {
            $service = new Service();
            $service->setName($lipsum->words(12));
            $service->genCanonicalUrl();
            $service->setImg('url_ '.$i);
            $service->setType(ServiceType::cases()[array_rand(ServiceType::cases())]);
            $manager->persist($service);

            for ($j= 0; $j < rand(3,12); $j++)
            {
                $serviceItem = new ServiceItem();
                $serviceItem->setName($lipsum->words(10));
                $serviceItem->setDescription($lipsum->words(20));
                $serviceItem->setPrice("От " . strval(rand(100, 120) * 100) . " руб");
                $serviceItem->setService($service);
                $manager->persist($serviceItem);
            }
            
        }

        $manager->flush();
    }
}
