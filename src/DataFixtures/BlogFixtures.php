<?php

namespace App\DataFixtures;

use App\Entity\Blog;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class BlogFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE blog_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();

        for ($i = 0; $i < 250; $i++) {
            $blog = new Blog();
            $blog->setTitle($lipsum->words(12));
            $blog->genCanonicalUrl();
            $blog->setDescription($lipsum->words(25));
            $blog->setCreatedAt(new \DateTimeImmutable());
            $blog->setText($lipsum->words(1000));
            $manager->persist($blog);
        }

        $manager->flush();
    }
}
