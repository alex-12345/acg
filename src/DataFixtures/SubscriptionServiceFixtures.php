<?php

namespace App\DataFixtures;

use App\Entity\ServiceItem;
use App\Entity\ServiceItemSubscriptionService;
use App\Entity\SubscriptionService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class SubscriptionServiceFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE subscription_service_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();
        $repo = $manager->getRepository(ServiceItem::class);
        $serviceItems = $repo->findAll();
        for ($i = 0; $i < 6; $i++) {
            $subscriptionService = new SubscriptionService();
            $subscriptionService->setName($lipsum->words(4));
            $subscriptionService->setSubTitle($lipsum->words(5));
            $subscriptionService->setDescription($lipsum->words(20));
            $subscriptionService->setPrice("От " . strval(rand(100, 120) * 100) . " ₽");
            $subscriptionService->setImg('url_ '.$i);


            $manager->persist($subscriptionService);

            $locServiceItems = array_rand($serviceItems, rand(3,6));

            foreach($locServiceItems as $item)
            {
                $temp = new ServiceItemSubscriptionService();
                $temp->setServiceItemMap($repo->find($item));
                $temp->setSubscriptionServiceMap($subscriptionService);
                $temp->setAmount("x".strval(rand(3, 120)));
                $manager->persist($temp);

            }
        }
        $manager->flush();

        
    }

    public function getDependencies()
    {
        return [
            ServiceFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
         return ['group1'];
    }
}
