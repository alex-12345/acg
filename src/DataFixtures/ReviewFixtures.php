<?php

namespace App\DataFixtures;

use App\Entity\Review;
use App\Entity\ServiceItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ReviewFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE review_id_seq RESTART WITH 1");

        $repo = $manager->getRepository(ServiceItem::class);
        $serviceItems = $repo->findAll();

        for ($i = 0; $i < 150; $i++) {
            $review = new Review();
            $review->setAuthor('name_ '.$i);
            $review->setUrl('url_ '.$i);
            $review->setCompanyName('comp_name_ '.$i);
            $review->setCreatedAt(new \DateTimeImmutable());
            $review->setText('description_ '.$i);
            $serviceItem = $serviceItems[rand(0, count($serviceItems)-20)];
            $review->setServiceItem($serviceItem);
            $manager->persist($review);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ServiceFixtures::class,
        ];
    }
}
