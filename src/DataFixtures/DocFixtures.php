<?php

namespace App\DataFixtures;

use App\Entity\Doc;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DocFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE doc_id_seq RESTART WITH 1");

        for ($i = 0; $i < 5500; $i++) {
            $doc = new Doc();
            $doc->setName('name_ '.$i);
            $doc->setUrl('url_ '.$i);
            $doc->setCreatedAt(new \DateTimeImmutable());
            $doc->setDescription('description_ '.$i);
            $manager->persist($doc);
        }

        $manager->flush();
    }
}
