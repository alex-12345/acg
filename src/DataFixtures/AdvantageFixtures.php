<?php

namespace App\DataFixtures;

use App\Entity\Advantage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class AdvantageFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE advantage_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();

        for ($i = 0; $i < 3; $i++) {
            $advantage = new Advantage();
            $advantage->setText($lipsum->words(200));
            $advantage->setTitle($lipsum->words(20));
            $advantage->setImg('url_ '.$i);
            $manager->persist($advantage);

        }
        $manager->flush();

        
    }
}
