<?php

namespace App\DataFixtures;

use App\Entity\Request;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class RequestFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->exec("ALTER SEQUENCE request_id_seq RESTART WITH 1");
        $lipsum = new LoremIpsum();

        for ($i = 0; $i < 250; $i++) {
            $request = new Request();
            $request->setClientName($lipsum->words(2));
            $request->setPhone("+7" . strval(rand(91500000000, 9999999999)));
            $request->setCreatedAt(new \DateTimeImmutable());
            $request->setText($lipsum->words(100));
            $manager->persist($request);
        }

        $manager->flush();
    }
}
