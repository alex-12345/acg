<?php
declare(strict_types=1);

namespace App\Events;

use App\Entity\Request as ClientRequest;
use Symfony\Contracts\EventDispatcher\Event;

class NewRequestEvent extends Event
{
    protected ClientRequest $clientRequest;

    public function __construct(ClientRequest $clientRequest)
    {
        $this->clientRequest = $clientRequest;;
    }

    public function getClientRequest():ClientRequest
    {
        return $this->clientRequest;
    }

}