<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230326013456 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE service_item_subscription_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE service_item_subscription_service (id INT NOT NULL, service_item_map_id INT DEFAULT NULL, subscription_service_map_id INT DEFAULT NULL, amount VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BD5F749719A60C07 ON service_item_subscription_service (service_item_map_id)');
        $this->addSql('CREATE INDEX IDX_BD5F749747037496 ON service_item_subscription_service (subscription_service_map_id)');
        $this->addSql('ALTER TABLE service_item_subscription_service ADD CONSTRAINT FK_BD5F749719A60C07 FOREIGN KEY (service_item_map_id) REFERENCES service_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_item_subscription_service ADD CONSTRAINT FK_BD5F749747037496 FOREIGN KEY (subscription_service_map_id) REFERENCES subscription_service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE service_item_subscription_service_id_seq CASCADE');
        $this->addSql('ALTER TABLE service_item_subscription_service DROP CONSTRAINT FK_BD5F749719A60C07');
        $this->addSql('ALTER TABLE service_item_subscription_service DROP CONSTRAINT FK_BD5F749747037496');
        $this->addSql('DROP TABLE service_item_subscription_service');
    }
}
