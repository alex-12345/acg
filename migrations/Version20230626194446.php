<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230626194446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_item ADD review_id INT NOT NULL');
        $this->addSql('ALTER TABLE service_item ADD CONSTRAINT FK_D15891F23E2E969B FOREIGN KEY (review_id) REFERENCES review (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D15891F23E2E969B ON service_item (review_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE service_item DROP CONSTRAINT FK_D15891F23E2E969B');
        $this->addSql('DROP INDEX IDX_D15891F23E2E969B');
        $this->addSql('ALTER TABLE service_item DROP review_id');
    }
}
