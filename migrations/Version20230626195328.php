<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230626195328 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_item DROP CONSTRAINT fk_d15891f23e2e969b');
        $this->addSql('DROP INDEX idx_d15891f23e2e969b');
        $this->addSql('ALTER TABLE service_item DROP review_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE service_item ADD review_id INT NOT NULL');
        $this->addSql('ALTER TABLE service_item ADD CONSTRAINT fk_d15891f23e2e969b FOREIGN KEY (review_id) REFERENCES review (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d15891f23e2e969b ON service_item (review_id)');
    }
}
