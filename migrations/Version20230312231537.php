<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230312231537 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE jsonblock_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE jsonblock (id INT NOT NULL, key VARCHAR(255) NOT NULL, data TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7A2BBFE8A90ABA9 ON jsonblock (key)');
        $this->addSql('ALTER TABLE review ALTER text TYPE TEXT');
        $this->addSql('ALTER TABLE review ALTER text TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE jsonblock_id_seq CASCADE');
        $this->addSql('DROP TABLE jsonblock');
        $this->addSql('ALTER TABLE review ALTER text TYPE VARCHAR(1000)');
    }
}
