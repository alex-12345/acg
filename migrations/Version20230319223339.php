<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230319223339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_e19d9ad2f47645ae');
        $this->addSql('ALTER TABLE service RENAME COLUMN url TO canonical_url');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E19D9AD2CB9C4093 ON service (canonical_url)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_E19D9AD2CB9C4093');
        $this->addSql('ALTER TABLE service RENAME COLUMN canonical_url TO url');
        $this->addSql('CREATE UNIQUE INDEX uniq_e19d9ad2f47645ae ON service (url)');
    }
}
