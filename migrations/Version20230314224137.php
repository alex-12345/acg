<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230314224137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person ALTER brief_info TYPE TEXT');
        $this->addSql('ALTER TABLE person ALTER brief_info TYPE TEXT');
        $this->addSql('ALTER TABLE person ALTER qualification TYPE TEXT');
        $this->addSql('ALTER TABLE person ALTER qualification TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE person ALTER brief_info TYPE VARCHAR(2500)');
        $this->addSql('ALTER TABLE person ALTER qualification TYPE VARCHAR(2500)');
    }
}
