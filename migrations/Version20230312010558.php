<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230312010558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE blog ADD canonical_url VARCHAR(550) NOT NULL');
        $this->addSql('ALTER TABLE blog ALTER title TYPE VARCHAR(500)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C0155143CB9C4093 ON blog (canonical_url)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_C0155143CB9C4093');
        $this->addSql('ALTER TABLE blog DROP canonical_url');
        $this->addSql('ALTER TABLE blog ALTER title TYPE VARCHAR(255)');
    }
}
