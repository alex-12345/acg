<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230319202718 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_item ADD service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_item ADD CONSTRAINT FK_D15891F2ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D15891F2ED5CA9E6 ON service_item (service_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE service_item DROP CONSTRAINT FK_D15891F2ED5CA9E6');
        $this->addSql('DROP INDEX IDX_D15891F2ED5CA9E6');
        $this->addSql('ALTER TABLE service_item DROP service_id');
    }
}
