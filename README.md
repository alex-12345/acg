# ACG #

## Начало работы ##

#### Установка пакетов ####
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  composer install
```

## Запуск ##
#### Выполнение миграций и загрузка необходимых данных в БД ####
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  php-fpm php bin/console doctrine:migrations:migrate
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  php-fpm php bin/console doctrine:fixtures:load --group=release # загрузим JSON блоки
```

#### Запуск ####
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose up --remove-orphans -d nginx
```
После выполнения команды проект будет доступен по адресу [amazon-city.localhost](http://amazon-city.localhost)

#### Опционально (пароль от админки) ####
Пароль задается в .env файле в виде хэша. Хэш можно получить так
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  php-fpm php bin/console security:hash-password qwerty_pass
```

#### Опционально ssl ####
Выпустить сертификат (выполняется единожды - после выполнения раскомментируйте вторую секцию в docker/nginx/sites-enabled/amazon-city.localhost.conf)
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d amazon-city.su -d www.amazon-city.su
```
Обновить
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm certbot renew
```

#### Дампы БД ####
Сделать дамп 
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker exec -it acg-postgres-1 /usr/bin/pg_dump -U amazon_city_user -d amazon_city > docker/postgre/dump.sql
```
Загрузить дамп
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm  php-fpm php bin/console doctrine:schema:drop --force
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker exec acg-postgres-1 /usr/bin/psql -U amazon_city_user -d amazon_city -f /home/dump.sql
```

#### Оптимизация ####
Закэшровать все классы 
```bash
export _UID="$(id -u)" && export _GID="$(id -g)"  &&  docker-compose run --rm composer dump-autoload  --classmap-authoritative
```